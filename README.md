# HRI-PPS

Human-Robot Interaction experiments on peripersonal space, proxemics, and touch with Nao, Pepper robots.

- Analysis folder contains generated graphs and codes to generate those graphs.
- NAO-1 folder is from the first proxemics experiment without touch scenarios. Folders NAO-2 and Pepper are from the newer updated experiments and with added touch scenario.
- NAO-Red-Dejvice folder is from an event in Dejvice, however it was never executed. The scenario consits of gaze only.
- Support folder contains Behavior code, aruco markers and external code.

Those folders contain descriptions about their structure.

Thesis is avaliable at: https://dspace.cvut.cz/handle/10467/92799. Results from the proxemics experiment are avaliable at: https://arxiv.org/abs/2009.01818.
