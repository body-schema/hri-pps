#!/usr/bin/env python2
# -*- coding: utf-8 -*-
# Author: Adam Rojik

##### Imports
import sys, os
import argparse, json
import qi
from naoqi import ALProxy
import numpy as np
import datetime as dt, time
import cv2
from collections import deque


# Make modules accessible
CURRENT_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.append(CURRENT_DIR + "/modules")

from modules.logger.logger import logger

from modules.movement.movement import movement
from modules.camera.camera import camera
from modules.video.video import video
from modules.keypoints.keypoints import keypoints
from modules.calibration.rigid_transform_3D import rigid_transform_3D as findTransformation


class config(object):
   def __init__(self):
      # DEFAULTs
      self.scenes = {
         1: {"desc": "Control"
            },
         2: {"desc": "Robot",
               "zones": [0.16, 0.42, 1.27]
            },
         3: {"desc": "Human",
               "zones": [0.45, 1.2, 3.7]
            }
      }

      self.sceneSequence = self.scenes.keys()
      
      self.SCENE = self.sceneSequence[0]
      self.ZONE = None

      # Robot connection
      self.robIP = "asterix.local"
      self.robPORT = 9559

      # Robot - camera calibration
      self.toRobRotation = np.eye(3)
      self.toRobTranslation = np.array([0, 0, 0])
      self.calibrated = False
      self.calibrationEndEffector = "LArm"
      
      # OpenPose configuration
      self.opConf = {}
      self.opConf["model_folder"] = None # Filled from openpose path
      self.opConf["net_resolution"] = "-1x144"
      self.opConf["number_people_max"] = 1
      self.opConf["tracking"] = 0

      # Camera configuration
      self.camConf = {
         'pathSettings': CURRENT_DIR+'/modules/camera/cameraSettings.json',
         'width': 1280,
         'height': 720,
         'fps': 15
      }

      # Keypoints configuration
      self.kpConf = {
         'kpPerson': [0, 1, 8, 15, 16, 17, 18],
         'kpRobot': ['CameraTop'],

         'kpGaze': [0, 16, 15, 18, 17], # Ordered list by priority
         'kpEye': 'CameraTop',
         
         'samples': 3
      }

      self.get2Dto3D = None
      self.get3Dto2D = None

      self.FRAME = 1

      self.logger = None
      self.camera = None
      
      self.memory = None
      self.startTime = time.time()

conf = config()
parser = argparse.ArgumentParser(description='Robot scenarios implementation.')
parser.add_argument('-x', '--set', type=int,
                    help='load X-th scene from sequence based on ID (starting from 0) '+str(conf.sceneSequence))
parser.add_argument('-i', '--id', type=int,
                    help='set persons ID (sets output name, generates sequence)')
parser.add_argument('-s', '--scene', type=int,
                    help='load scene (overwrites load from set) ['+(", ".join([str(i)+":"+conf.scenes[i]["desc"] for i in conf.scenes]))+']')
parser.add_argument('-o', '--output', type=str,
                    help='record scenario to given output folder in "recordings/", will not record by default (eg. "0123/")')
parser.add_argument('-c', '--calibration', type=str,
                    help='use exported calibration settings ("'+json.dumps([conf.toRobRotation.tolist(), conf.toRobTranslation.tolist()])+'")')
parser.add_argument('-a', '--address', type=str, default='asterix.local',
                    help='set robots ip address')
parser.add_argument('-p', '--port', type=int, default=9559,
                    help='set robots port')
opt = parser.parse_args()

willExit = False
if opt != {}:
   # Set person id
   if opt.id != None:
      conf.sceneSequence = [(opt.id+2)%3+1]
      print("Setting output and scene sequence: "+str(conf.sceneSequence))
      opt.output = str(opt.id)+"/"

   # Set sene from sequence
   if len(conf.sceneSequence) == 1:
      conf.SCENE = conf.sceneSequence[0]
      print("Setting scene: "+str(conf.SCENE))
   if opt.set != None:
      if opt.set not in range(1,len(conf.sceneSequence)+1):
         print(str(opt.set)+"-th scene is not in set")
         willExit = True
      else:
         conf.SCENE = conf.sceneSequence[opt.set - 1]
         print("Setting "+str(opt.set)+"-th scene from set to: "+str(conf.SCENE)+":"+conf.scenes[conf.SCENE]["desc"])

   # Set scene
   if opt.scene != None:
      if opt.scene not in conf.scenes.keys():
         print("Scene "+str(opt.scene)+" does not exist.")
         willExit = True
      else:
         conf.SCENE = opt.scene
         print("Setting scene: "+str(conf.SCENE)+":"+conf.scenes[conf.SCENE]["desc"])
   
   # Setup logger
   if opt.output != None:
      opt.output = "recordings/" + opt.output + time.strftime("%y-%m-%d_%H-%M-%S")+"_SCENE-"+str(conf.SCENE)+"-"
      print("Setting output to: "+opt.output)
      conf.logger = logger(opt.output)
   else:
      conf.logger = logger()

   # Set calibration
   if opt.calibration != None:
      try:
         calibration = json.loads(opt.calibration)
         conf.toRobRotation = np.array(calibration[0])
         conf.toRobTranslation = np.array(calibration[1])
         conf.toRobRotation.shape = (3,3)
         conf.calibrated = True
      
         print("Imported calibration.")
      except:
         print("Unable to process calibration string, ignoring argument.")
         willExit = True
   
   if opt.address != None:
      conf.robIP = opt.address
   
   if opt.port != None:
      conf.robPORT = opt.port

if willExit:
   sys.exit(0)

print("Running scene "+str(conf.SCENE)+":"+conf.scenes[conf.SCENE]["desc"])
print("Are the settings correct? (waiting for a second)")
time.sleep(1)

# CV2 View setup
WINDOW_NAME = 'Scenario '+str(conf.SCENE)+':'+conf.scenes[conf.SCENE]["desc"]

loggerJoints = conf.logger.createWriter("jointsBody", timed = True, fileLog = True)

# Init random
np.random.seed()
videoWriter = None
try:
   with movement(conf) as mv:
      with camera(conf) as cam:
         vid = video(conf)
         cv2.startWindowThread()
         cv2.namedWindow(WINDOW_NAME, cv2.WINDOW_NORMAL)

         mv.setStiffnesses("Arms", 1)
         # Callibration
         if not conf.calibrated:
            mv.do("relax")
            mv.setStiffnesses("RArm", 0)
            mv.setStiffnesses("Legs", 1)
            print("Not calibrated, running calibration.")
            markerCam3D = []
            markerRob3D = []
            for i in range(9):
               mv.do("calibration"+str(i))
               time.sleep(1)

               while True:
                  cam.getFrame()
                  markerCenter = vid.findMarkerCenter(cam.color_image)
 
                  cv2.imshow(WINDOW_NAME, cam.color_image)
                  key = cv2.waitKey(33)
                  # Press esc or 'q' to close the image window
                  if key & 0xFF == ord('q') or key == 27:
                     cv2.destroyAllWindows()
                     sys.exit(0)

                  if not markerCenter is None:
                     markerCenter3D = conf.get2Dto3D(markerCenter)
                     if markerCenter3D[2] > 0:
                        markerCam3D.append(markerCenter3D)
                        markerRob3D.append(mv.getPosition(conf.calibrationEndEffector)[:3])
                        print("Found marker for calibration "+str(i))
                        break

            A = np.array(markerCam3D)
            B = np.array(markerRob3D)
            conf.toRobRotation, conf.toRobTranslation = findTransformation(A,B)
            print("Calibration done: \""+json.dumps([conf.toRobRotation.tolist(), conf.toRobTranslation.tolist()])+"\"")
            conf.calibrated = True
            mv.setStiffnesses("Legs", 0)
            mv.setStiffnesses("RArm", 1)

         loggerCalibration = conf.logger.createWriter("calibration", fileLog = True)
         loggerCalibration.verbose(json.dumps([conf.toRobRotation.tolist(), conf.toRobTranslation.tolist()]))

         mv.do("relax")
         mv.rest()
         mv.initHead()
         kp = keypoints(conf, mv)

         # Pre-event setup
         zone = None
         if "zones" in conf.scenes[conf.SCENE]:
            conf.ZONE = conf.scenes[conf.SCENE]["zones"]
            zone = len(conf.ZONE) - 1
            zoneWindow = [(zone, time.time())]

         cam.getFrame()
         futureDatum = qi.async(vid.findPerson, cam.color_image)

         # Video recording
         videoTimer = time.time()
         videoFrames = 0
         if conf.logger.exportPath != "":
            videoWriter = cv2.VideoWriter(conf.logger.exportPath + 'video.avi', cv2.VideoWriter_fourcc(*'x264'), float(conf.camConf['fps']), (conf.camConf['width'], conf.camConf['height']))

         zoneVelocity = 0
         randomActionTime = time.time()
         randomBlinkTime = time.time()
         while True:
            # Video processing
            t = time.time()
            if futureDatum.isFinished():
               cam.getFrame()
               loggerJoints.verbose(str(mv.motion.getAngles("Body", True)))
               futureDatum = qi.async(vid.findPerson, cam.color_image)

               # Processes keypoints
               outputImage, outputDistance, outputLookat = kp.guiOnDatum(vid.datum)
               if not outputDistance is None and not conf.ZONE is None:
                  for id, zoneMax in enumerate(conf.ZONE):
                     if outputDistance < zoneMax:
                        zoneVelocity = zone - id
                        zone = id
                        break
                     else:
                        if id == len(conf.ZONE):
                           zoneVelocity = zone - len(conf.ZONE) - 1
                           zone = len(conf.ZONE) - 1
                  zoneWindow.append((id, t))
                  while len(zoneWindow) > 0 and zoneWindow[0][1] < t - 1.5:
                     del zoneWindow[0]
                  zone = min(zoneWindow, key=lambda x:x[0])[0]

               if not conf.ZONE is None:
                  # Zone actions
                  # Set lookat
                  if not outputLookat is None:
                     if zone <= 1:
                        mv.setAngles("Head", mv.getLookatAngles(outputLookat))
                     else:
                        mv.setAngles("Head", [0]*2)
               else:
                  # Random point to lookat
                  if t > randomActionTime:
                     randomActionTime = time.time() + np.random.normal(loc=5.0, scale=1.0, size=None)
                     x = np.random.random()*4-2
                     y = np.random.random()*2

                     mv.setAngles("Head", mv.getLookatAngles([3, x, y]))


               text = "Zone: "+str(zone)
               cv2.putText(outputImage, text, (10,40), cv2.FONT_HERSHEY_DUPLEX, 1.1, (0,0,0), 3)
               cv2.putText(outputImage, text, (10,40), cv2.FONT_HERSHEY_DUPLEX, 1.1, (255,255,255), 2)
               # Show view with updated actions
               if not outputImage is None:
                  cv2.imshow(WINDOW_NAME, outputImage)

                  if not videoWriter is None:
                     timer = time.time() - videoTimer
                     while videoFrames < timer*conf.camConf['fps']:
                        videoWriter.write(outputImage)
                        videoFrames += 1
                        timer = time.time() - videoTimer
            
            if t > randomBlinkTime:
               mv.blink()
               randomBlinkTime = time.time() + np.random.normal(loc=3.4, scale=1, size=None)
            
            key = cv2.waitKey(33)
            # Press esc or 'q' to close the image window
            if key & 0xFF == ord('q') or key == 27:
               cv2.destroyAllWindows()
               break

finally:
   if not videoWriter is None:
      videoWriter.release()
