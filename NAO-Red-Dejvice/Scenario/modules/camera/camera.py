#!/usr/bin/env python2
# -*- coding: utf-8 -*-
# Author: Adam Rojik

##### Imports
import pyrealsense2 as rs
import json
import numpy as np
import math


class camera(object):
   def __init__(self, conf):
      self.conf = conf

      self.logger = self.conf.logger.createWriter("camera", fileLog=False, timed=True)

      # Load camera advanced settings
      ctx = rs.context()
      devices = ctx.query_devices()
      dev = devices[0]
      advnc_mode = rs.rs400_advanced_mode(dev)

      while not advnc_mode.is_enabled():
         self.logger.info("Activating advanced mode")
         advnc_mode.toggle_advanced_mode(True)
         time.sleep(5)

      with open(self.conf.camConf['pathSettings'], 'r') as json_file:
         as_json_object = json.load(json_file)

      # For Python 2, the values in 'as_json_object' dict need to be converted from unicode object to utf-8
      if type(next(iter(as_json_object))) != str:
         as_json_object = {k.encode('utf-8'): v.encode("utf-8") for k, v in as_json_object.items()}
      # The C++ JSON parser requires double-quotes for the json object so we need
      # to replace the single quote of the pythonic json to double-quotes
      json_string = str(as_json_object).replace("'", '\"')
      advnc_mode.load_json(json_string)

      #Create a config and configure the pipeline to stream
      # color and depth streams - should be kept same, otherwise projection won't work
      config = rs.config()
      config.enable_stream(rs.stream.depth, self.conf.camConf['width'], self.conf.camConf['height'], rs.format.z16, self.conf.camConf['fps'])
      config.enable_stream(rs.stream.color, self.conf.camConf['width'], self.conf.camConf['height'], rs.format.bgr8, self.conf.camConf['fps'])

      if self.conf.logger.exportPath != "":
         config.enable_record_to_file(self.conf.logger.exportPath + 'raw.bag')
 
      # Create a pipeline
      self.pipeline = rs.pipeline()

      # Start streaming
      profile = self.pipeline.start(config)

      # Getting the depth sensor's depth scale (required for 3D <-> 2D transformation)
      depth_sensor = profile.get_device().first_depth_sensor()
      self.depth_scale = depth_sensor.get_depth_scale()
      self.depth_intrin = None

      # Create an align object
      # rs.align allows us to perform alignment of depth frames to others frames
      # The "align_to" is the stream type to which we plan to align depth frames.
      align_to = rs.stream.color
      self.align = rs.align(align_to)
      
      for i in range(10): # Throw away first 10 frames
         self.getFrame()
         
      conf.get2Dto3D = self.get2Dto3D
      conf.get3Dto2D = self.get3Dto2D


   def getFrame(self):
      """Returns color image, depth image as numpy arrays."""
      aligned_depth_frame = False
      aligned_color_frame = False

      # Validate that both frames are valid
      while not aligned_depth_frame or not aligned_color_frame:
         frames = self.pipeline.wait_for_frames()

         # Align the depth frame to color frame
         aligned_frames = self.align.process(frames)

         # Get aligned frames
         aligned_depth_frame = aligned_frames.get_depth_frame()
         aligned_color_frame = aligned_frames.get_color_frame()

      # Intrinsics
      self.depth_intrin = aligned_depth_frame.profile.as_video_stream_profile().intrinsics

      # Get depth and color values in a np array
      self.depth_image = np.asanyarray(aligned_depth_frame.get_data())
      self.color_image = np.asanyarray(aligned_color_frame.get_data())
      
      return self.color_image, self.depth_image


   def get2Dto3D(self, xy, area=0, depth_image = None):
      """From 2D (x,y) return 3D numpy array x,y,z from cameras point of view.
      Requires data from getFrame, so it must be called first, otherwise returns None."""
      if self.depth_intrin == None:
         self.logger.error("getFrame must be called before get2Dto3D.")
         return None
      
      x = xy[0]
      y = xy[1]
      if x < 0 or y < 0:
         return np.array([0, 0, 0])
      
      if depth_image == None:
         depth_image = self.depth_image
   
      updatedX = min(depth_image.shape[1] - 1, int(math.floor(x)))
      updatedY = min(depth_image.shape[0] - 1, int(math.floor(y)))
      z = depth_image[max((updatedY - area), 0):min((updatedY + area + 1), depth_image.shape[0]), max((updatedX-area),0):min((updatedX+area+1), depth_image.shape[1])].min() * self.depth_scale
      return np.array(rs.rs2_deproject_pixel_to_point(self.depth_intrin, [float(x), float(y)], z))
      
   
   def get3Dto2D(self, xyz):
      """From 3D numpy array x,y,z return (x,y) projected to 2D cameras point of view.
      Requires data from getFrame, so it must be called first, otherwise returns None."""
      if self.depth_intrin == None:
         self.logger.error("getFrame must be called before get3Dto2D.")
         return None

      return rs.rs2_project_point_to_pixel(self.depth_intrin, xyz.tolist())


   def __enter__(self):
      return self


   def __exit__(self, exc_type, exc_value, traceback):
      self.pipeline.stop()
