#!/usr/bin/env python2
# -*- coding: utf-8 -*-
# Author: Adam Rojik

##### Imports
import numpy as np
import time, datetime as dt
import math
import cv2


def intTuple(xy):
   return (int(xy[0]), int(xy[1]))

class keypoints(object):
   def __init__(self, conf, motion):
      self.conf = conf.kpConf
      self.loggerKeypoints2D = conf.logger.createWriter("keypoints2D", timed = True, fileLog = True)
      self.loggerKeypointsDistances = conf.logger.createWriter("keypointsDistances", timed = True, fileLog = True)
      self.loggerKeypointsGaze = conf.logger.createWriter("keypointsGaze", timed = True, fileLog = True)

      self.toRobTranslation = conf.toRobTranslation
      self.toRobRotation = conf.toRobRotation
      self.get2Dto3D = conf.get2Dto3D
      self.get3Dto2D = conf.get3Dto2D
      self.mv = motion

      self.transformations = {}
      for kpName in self.conf['kpRobot']:
         self.transformations[kpName] = np.array(motion.getPosition(kpName, 2, True)[:3]) - np.array(motion.getPosition("Torso", 2, True)[:3])

      self.samplesGaze = []
      self.samples = []


   def guiOnDatum(self, datum):
      # Find nose/head, closest collision object and draw them to datum.cvOutputData
      # Export zone + distance of collision object
      output = datum.cvOutputData
      outputDistance = None
      outputLookat = None
      
      if datum.poseKeypoints.size > 1:
         self.loggerKeypoints2D.verbose(str(datum.poseKeypoints[0]))
         robot2D = intTuple(self.get3Dto2D(np.dot(np.linalg.inv(self.toRobRotation), (self.toRobTranslation))))
         cv2.circle(output, robot2D, 5, (0,0,255), 5)

         # PPS
         robot3DKeypoints = []
         for kpName in self.conf['kpRobot']:
            robot3D = self.transformations[kpName] + np.array(self.mv.getPosition("Torso", 1, False)[:3])
            robot2D = intTuple(self.get3Dto2D(np.dot(np.linalg.inv(self.toRobRotation), (robot3D-self.toRobTranslation))))
            cv2.circle(output, robot2D, 5, (0,0,255), 5)
            robot3DKeypoints.append((robot3D, robot2D))
      
         dist = None
         distances = []
         for kpID in self.conf['kpPerson']:
            keypoint = datum.poseKeypoints[0][kpID]
            if keypoint[2] > .2:
               keypoint3D = self.get2Dto3D(keypoint[:2], area=6)
               keypoint3DRobotFrame = np.dot(self.toRobRotation, keypoint3D) + self.toRobTranslation
               keypoint2D = intTuple(keypoint[:2])
               cv2.circle(output, keypoint2D, 5, (0,0,0), 5)

               # Check distance from robot keypoints
               for robotKP in robot3DKeypoints:
                  kdist = np.linalg.norm(robotKP[0] - keypoint3DRobotFrame)
                  distances.append(kdist)
                  if dist is None or kdist < dist[0]:
                     dist = (kdist, robotKP[1], keypoint2D, keypoint3DRobotFrame)

         if not dist is None:
            if len(self.samples) > self.conf['samples']:
               del self.samples[0]
            self.samples.append(dist[0])
         
         if not dist is None and len(self.samples) > 2 and np.var(self.samples, ddof=1) < .2:
            outputDistance = dist[0]
            cv2.arrowedLine(output, dist[1], dist[2], (255, 0, 0), int(10*dist[0])+1, tipLength = 0.01+0.05*dist[0])

            text = "|"+str(round(dist[0],2))+"| m"
            cv2.putText(output, text, dist[2], cv2.FONT_HERSHEY_DUPLEX, 2, (0,0,0), 5)
            cv2.putText(output, text, dist[2], cv2.FONT_HERSHEY_DUPLEX, 2, (255,255,255), 4)

            text = "Closest: "+str(map(round, dist[3], len(dist[3])*[2]))
            cv2.putText(output, text, (10,120), cv2.FONT_HERSHEY_DUPLEX, 1.1, (0,0,0), 3)
            cv2.putText(output, text, (10,120), cv2.FONT_HERSHEY_DUPLEX, 1.1, (255,255,255), 2)


         # Gaze
         neck = datum.poseKeypoints[0][1]
         if neck[2] > .2:
            eyePosition = np.array(self.mv.getPosition(self.conf['kpEye'])[:3])
            eye2D = intTuple(self.get3Dto2D(np.dot(np.linalg.inv(self.toRobRotation), (eyePosition-self.toRobTranslation))))
   
            neck3D = self.get2Dto3D(neck[:2])

            keypoint = None
            for kpID in self.conf['kpGaze']:
               kp = datum.poseKeypoints[0][kpID]
               if kp[2] > .2:
                  keypoint3D = self.get2Dto3D(kp[:2])
                  keypoint2D = intTuple(kp[:2])
                  keypoint3DRobotFrame = np.dot(self.toRobRotation, keypoint3D) + self.toRobTranslation
                  dist = np.linalg.norm(keypoint3DRobotFrame - eyePosition)
               
                  distDiff = np.linalg.norm(keypoint3D - neck3D)
                  if distDiff < .4:
                     keypoint = (dist, keypoint2D, keypoint3DRobotFrame)
                     break
         
            if not keypoint is None and keypoint[0] > .1:
               if len(self.samplesGaze) > self.conf['samples']:
                  del self.samplesGaze[0]
               self.samplesGaze.append(keypoint[0])
   
               if len(self.samplesGaze) > 2 and np.var(self.samplesGaze, ddof=1) < .1:
                  cv2.circle(output, keypoint[1], 5, (0,255,0), 5)
                  cv2.circle(output, eye2D, 5, (0,255,0), 5)
                  cv2.arrowedLine(output, eye2D, keypoint[1], (0, 255, 0), int(10*dist)+1, tipLength = 0.01+0.05*dist)

                  text = "|"+str(round(keypoint[0],2))+"| m"
                  cv2.putText(output, text, keypoint[1], cv2.FONT_HERSHEY_DUPLEX, 2, (0,0,0), 5)
                  cv2.putText(output, text, keypoint[1], cv2.FONT_HERSHEY_DUPLEX, 2, (255,255,255), 4)

                  text = "Eyes: "+str(map(round, keypoint[2], len(keypoint[2])*[2]))
                  cv2.putText(output, text, (10,80), cv2.FONT_HERSHEY_DUPLEX, 1.1, (0,0,0), 3)
                  cv2.putText(output, text, (10,80), cv2.FONT_HERSHEY_DUPLEX, 1.1, (255,255,255), 2)

                  outputLookat = keypoint[2].tolist()

         text = dt.datetime.now().strftime("%Y.%m.%d %H:%M:%S")
         cv2.putText(output, text, (10,700), cv2.FONT_HERSHEY_DUPLEX, 1.1, (0,0,0), 3)
         cv2.putText(output, text, (10,700), cv2.FONT_HERSHEY_DUPLEX, 1.1, (255,255,255), 2)

      self.loggerKeypointsDistances.verbose(str(outputDistance))
      self.loggerKeypointsGaze.verbose(str(outputLookat))
      return output, outputDistance, outputLookat
