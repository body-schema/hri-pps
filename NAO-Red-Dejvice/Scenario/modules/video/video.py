#!/usr/bin/env python2
# -*- coding: utf-8 -*-
# Author: Adam Rojik

##### Imports
import os
import cv2
import pyopenpose as op
import copy
import time


class video(object):
   """Video post-processing class."""
   def __init__(self, conf):
      self.op = None
      self.conf = conf
      if self.conf.opConf["model_folder"] == None:
         self.conf.opConf["model_folder"] = "/".join(op.__file__.split("/")[:-4])+"/models"
      self.datum = None


   def findPerson(self, image):
      """OpenPose processed image and returns datum value from it if keypoints were found.
      For better preformance should be run in different thread.
      Returns None if not found."""
      if self.op == None:
         self.op = op.WrapperPython()
         self.op.configure(self.conf.opConf)
         self.op.start()
      
      datum = op.Datum()
      datum.cvInputData = image
      self.op.emplaceAndPop([datum])
      self.datum = datum


   def findMarkerCenter(self, image):
      """Finds Aruco markers center withing the image and returns its x,y position as numpy array.
      Returns None if not found."""
      markers = cv2.aruco.detectMarkers(image, cv2.aruco.getPredefinedDictionary(cv2.aruco.DICT_7X7_50))
      r = None
      if len(markers[0]) > 0:
         r = markers[0][0][0,:,:].sum(axis=0)*0.25
      
      return r
      