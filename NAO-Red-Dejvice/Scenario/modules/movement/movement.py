#!/usr/bin/env python2
# -*- coding: utf-8 -*-
# Author: Adam Rojik

##### Imports
import numpy as np
from naoqi import ALProxy
import qi
import movementDB as mvdb
import math


class movement(object):
   """Movement. Anything you want to do with the robot, do it here."""
   def __init__(self, conf):
      
      self.FRAME = conf.FRAME

      self.memory = ALProxy("ALMemory", conf.robIP, conf.robPORT)
      self.motion = ALProxy("ALMotion", conf.robIP, conf.robPORT)
      self.leds = ALProxy("ALLeds", conf.robIP, conf.robPORT)

      self.loggerMove = conf.logger.createWriter("move", timed = True, fileLog = True)
      self.loggerLookat = conf.logger.createWriter("lookat", timed = True, fileLog = True)

      self.disableActions = False
      self.refHeadPosition = np.array(self.motion.getPosition("Head", self.FRAME, False)[:3])
      
      ledList = []
      for i in range(1,9):
         if not i in [3,4]:#,6
            ledList.append("LeftFaceLed"+str(i))
         if not i in [5,6]:#,3
            ledList.append("RightFaceLed"+str(i))
      self.leds.createGroup("FaceLedsUpper", ledList)


   def do(self, move):
      """Do a given movement from mvdb, can do interpolation if move is a list [moveA, moveB, ratio] (see mvdb.interpolate). Blocking call."""
      self.loggerMove.verbose(str(move))
      mvdata = None
      if type(move) is list:
         mvdata = mvdb.interpolate(mvdb.get(move[0]), mvdb.get(move[1]), float(move[2]))
      else:
         mvdata = mvdb.get(move)

      if not mvdata is None:
         self.motion.angleInterpolationBezier(mvdata["names"], mvdata["times"], mvdata["keys"])


   def setAngles(self, names, angles, speed = None):
      """Set angles to given values with max given fraction of a max speed. If speed is None, current angle difference makes up the speed."""
      if names == "Head":
         angles[0] = min(.58, max(-.5, angles[0]))
         angles[1] = min(.26, max(-.66, angles[1]))
      if speed is None:
         speed = min(.2, np.linalg.norm(np.array(self.motion.getAngles(names, True)) - np.array(angles))*.2)
      self.motion.setAngles(names,angles,speed)


   def blink(self):
      """Make NAO blink using his LEDs module."""
      timeOfABlink = .1
      qi.async(self.leds.fadeListRGB, "FaceLedsUpper", [0x00555555, 0x00555555, 0x00FFFFFF], [timeOfABlink, timeOfABlink+.02, 2*timeOfABlink])


   def getLookatAngles(self, position):
      """Position3D in robot reference frame. Returns in order of Head chain (yaw, pitch)."""
      position -= self.refHeadPosition
      yawGoal = math.atan2(position[1], position[0])
      pitchGoal = -math.atan2(position[2], position[0])
      return [yawGoal, pitchGoal]
 

   def setStiffnesses(self, joints, value):
      """Set joint stiffnesses."""
      return self.motion.setStiffnesses(joints, value)


   def getAngles(self, joints):
      """Return angle values of given joint/chains/.. in radians."""
      return self.motion.getAngles(joints, True)
   
   
   def getBodyNames(self, chain):
      """Return joint names for given chain."""
      return self.motion.getBodyNames(chain)
   
   
   def getPosition(self, joint, frame = None, useSensors = True):
      """Return 6D joint position in given frame of reference."""
      if frame == None:
         frame = self.FRAME
      return self.motion.getPosition(joint, frame, useSensors)
   
   
   def getTransform(self, joint, frame = None, useSensors = True):
      """Return 4x4 numpy homogenous transformation matrix."""
      if frame == None:
         frame = self.FRAME
      T = np.array(self.motion.getTransform(joint, frame, useSensors))
      T.shape = (4,4)
      return T


   def rest(self):
      """Go to sitting position and set stiffnesses to 0."""
      self.motion.rest()


   def initHead(self):
      """Init head stiffnesses and go to init position."""
      self.motion.setStiffnesses("Head", 1.)
      self.motion.angleInterpolationWithSpeed("Head", [0]*2, .1)   


   def __enter__(self):
      # Init leds
      self.leds.off("AllLeds")
      self.leds.fade("FaceLeds", 1, 1)
      return self
      
      
   def __exit__(self, exc_type, exc_value, traceback):
      self.motion.setStiffnesses("Head", 1.)
      self.motion.angleInterpolationWithSpeed("Head", [0]*2, .1)
      self.motion.setStiffnesses("Arms", 1.)
      self.do("relax")
      self.motion.rest()
