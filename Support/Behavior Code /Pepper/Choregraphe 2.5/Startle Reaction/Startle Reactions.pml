<?xml version="1.0" encoding="UTF-8" ?>
<Package name="Startle Reaction Left Side" format_version="4">
    <Manifest src="manifest.xml" />
    <BehaviorDescriptions>
        <BehaviorDescription name="behavior" src="Startle Reaction On Touch" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="Testing Components" xar="behavior.xar" />
    </BehaviorDescriptions>
    <Dialogs />
    <Resources />
    <Topics />
    <IgnoredPaths />
    <Translations auto-fill="en_US">
        <Translation name="translation_en_US" src="translations/translation_en_US.ts" language="en_US" />
    </Translations>
</Package>
