<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
    <context>
        <name>Startle Reaction On Touch/behavior.xar:/Say</name>
        <message>
            <location filename="Startle Reaction On Touch/behavior.xar" line="0"/>
            <source>Stopt it!!</source>
            <comment>Text</comment>
            <translation type="unfinished">Stopt it!!</translation>
        </message>
    </context>
    <context>
        <name>Startle Reaction On Touch/behavior.xar:/Say (1)</name>
        <message>
            <location filename="Startle Reaction On Touch/behavior.xar" line="0"/>
            <source>Hey!!</source>
            <comment>Text</comment>
            <translation type="unfinished">Hey!!</translation>
        </message>
    </context>
    <context>
        <name>Startle Reaction On Touch/behavior.xar:/Say (2)</name>
        <message>
            <location filename="Startle Reaction On Touch/behavior.xar" line="0"/>
            <source>Not again!</source>
            <comment>Text</comment>
            <translation type="unfinished">Not again!</translation>
        </message>
    </context>
    <context>
        <name>Startle Stimulus Left/behavior.xar:/Say</name>
        <message>
            <source>Hello</source>
            <comment>Text</comment>
            <translation type="vanished">Hello</translation>
        </message>
        <message>
            <source>Hey!</source>
            <comment>Text</comment>
            <translation type="obsolete">Hey!</translation>
        </message>
        <message>
            <source>Ouch</source>
            <comment>Text</comment>
            <translation type="obsolete">Ouch</translation>
        </message>
        <message>
            <source>Stop that!</source>
            <comment>Text</comment>
            <translation type="obsolete">Stop that!</translation>
        </message>
        <message>
            <source>Stopthat!</source>
            <comment>Text</comment>
            <translation type="obsolete">Stopthat!</translation>
        </message>
        <message>
            <source>Stoptit!!</source>
            <comment>Text</comment>
            <translation type="obsolete">Stoptit!!</translation>
        </message>
        <message>
            <source>Stopt it!!</source>
            <comment>Text</comment>
            <translation type="obsolete">Stopt it!!</translation>
        </message>
    </context>
    <context>
        <name>Startle Stimulus Left/behavior.xar:/Say (1)</name>
        <message>
            <source>Hey!!</source>
            <comment>Text</comment>
            <translation type="obsolete">Hey!!</translation>
        </message>
    </context>
    <context>
        <name>Startle Stimulus Left/behavior.xar:/Say (2)</name>
        <message>
            <source>Not again!</source>
            <comment>Text</comment>
            <translation type="obsolete">Not again!</translation>
        </message>
    </context>
</TS>
