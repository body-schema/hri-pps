#!/usr/bin/env python2
# -*- coding: utf-8 -*-
# Author: Adam Rojik

##### Imports
import numpy as np
import time
import qi
import movementDB as mvdb
import functools
import copy


class movement:
   """Movement. Anything you want to do with the robot, do it here."""
   def __init__(self, conf):
      session = conf.session
      
      self.FRAME = conf.FRAME

      self.session = session
      self.memory = session.service("ALMemory")
      self.motion = session.service("ALMotion")
      self.posture = session.service("ALRobotPosture")
      self.tracker = session.service("ALTracker")
      self.autonomous = session.service("ALAutonomousLife")
      self.autonomousBlinking = session.service("ALAutonomousBlinking")

      self.loggerTouch = conf.logger.createWriter("touch", timed = True, fileLog = True)
      self.loggerMove = conf.logger.createWriter("move", timed = True, fileLog = True)
      self.loggerLookat = conf.logger.createWriter("lookat", timed = True, fileLog = True)
      self.loggerMovement = conf.logger.createWriter("movement", timed = True, fileLog = False)

      self.eventList = {}
      self.touchEvents = {}
      
      self.move = None
      self.moveMotion = qi.async(lambda x:x)
      self.lookat = None
      self.lastLookat = None
      self.lookatNose = None # Helper for touch
      self.lookatMotion = qi.async(lambda x:x)
      
      self.disableActions = False # Helper variable for touch
      
      # Update autonomous life
      self.autonomous.setState("interactive")
      self.autonomous.setAutonomousAbilityEnabled("All", False)
      self.changedAutonomous = ["AutonomousBlinking"]
      for update in self.changedAutonomous: 
         self.autonomous.setAutonomousAbilityEnabled(update, True)
               

   def updateActions(self):
      """Coordinates to look at from given reference frame (as specified for the robot lookAt).
      By using coordinates from Robot frame (2), you don't need to worry about leg-joint angles."""
      if not self.move is None and self.moveMotion.isFinished():
         self.motion.setStiffnesses("Arms", .5)
         self.motion.setExternalCollisionProtectionEnabled("Arms", False)
         self.loggerMove.verbose(str(self.move))
         
         mvdata = None
         if type(self.move) is list:
            mvdata = mvdb.interpolate(mvdb.get(self.move[0]), mvdb.get(self.move[1]), float(self.move[2]))
         else:
            mvdata = mvdb.get(self.move)
         self.move = None

         if not mvdata is None:
            self.moveMotion = qi.async(self.motion.angleInterpolationBezier, mvdata["names"], mvdata["times"], mvdata["keys"])

      if not self.lookat is None and self.lookatMotion.isFinished():
         if self.lastLookat is None:
            self.lastLookat = self.lookat
         
         lookat = copy.deepcopy(self.lookat)
         frame = self.FRAME
         if type(self.lookat) is tuple:
            frame = self.lookat[1]
            self.lookat = self.lookat[0]
            newLookat = 1
         elif type(self.lastLookat) is tuple:
            newLookat = 1
         else:
            newLookat = np.array(self.lookat)-np.array(self.lastLookat)
         
         if np.linalg.norm(newLookat) > .05:
            self.loggerLookat.verbose(str(self.lookat))
            
            self.lastLookat = lookat
            self.lookatMotion = qi.async(self.tracker.lookAt, self.lookat, frame, float(max(.01, min(.15, np.linalg.norm(newLookat)*.4))), False)
         self.lookat = None

      
   def subscribeToEvent(self, eventName, signalID, callback):
      if eventName not in self.eventList.keys():
         self.eventList[eventName] = {
            "event": self.memory.subscriber(eventName),
            "subscribers": {}
         }
      self.loggerMovement.info("Subscribe to event:"+eventName+","+str(signalID)+","+str(callback))
      self.eventList[eventName]["subscribers"][signalID] = self.eventList[eventName]["event"].signal.connect(functools.partial(callback, eventName))
         
   
   def unsubscribeFromEvent(self, eventName, signalID):
      if eventName in self.eventList.keys() and signalID in self.eventList[eventName]["subscribers"].keys():
         self.eventList[eventName]["event"].signal.disconnect(self.eventList[eventName]["subscribers"][signalID])
         del self.eventList[eventName]["subscribers"][signalID]
         self.loggerMovement.info("Unsubscribe from event:"+eventName+","+str(signalID))


   def onTouchEvent(self, strVarName, touchedParts):      
      self.loggerTouch.verbose(strVarName+","+str(touchedParts))
      for touchedPart in touchedParts:
         if touchedPart[0] in self.touchEvents:
            if not self.lookatNose is None:
                  self.lookat = copy.deepcopy(self.lookatNose)

            if touchedPart[1]:
               self.move = self.touchEvents[touchedPart[0]]
               self.disableActions = False
               self.autonomousBlinking.setEnabled(True)
            else:
               self.move = "standnohead"
            break
               

   def stand(self):
      mvdata = mvdb.get("standnohead")
      self.motion.angleInterpolationBezier(mvdata["names"], mvdata["times"], mvdata["keys"])
      self.tracker.lookAt([5,0,1.2], 2, .3, False)


   def getAngles(self, joints):
      """Return angle values of given joint/chains/.. in radians."""
      return self.motion.getAngles(joints, True)
   
   
   def getBodyNames(self, chain):
      """Return joint names for given chain."""
      return self.motion.getBodyNames(chain)
   
   
   def getPosition(self, joint, frame = None, useSensors = True):
      """Return 6D joint position in given frame of reference."""
      if frame == None:
         frame = self.FRAME
      return self.motion.getPosition(joint, frame, useSensors)
   
   
   def getTransform(self, joint, frame = None, useSensors = True):
      if frame == None:
         frame = self.FRAME
      """Return 4x4 numpy homogenous transformation matrix."""
      T = np.array(self.motion.getTransform(joint, frame, useSensors))
      T.shape = (4,4)
      return T


   def __enter__(self):
      self.motion.wakeUp()
      return self
      
      
   def __exit__(self, exc_type, exc_value, traceback):
      self.posture.goToPosture("Stand", .3)
      #self.autonomous.setAutonomousAbilityEnabled("All", True)
