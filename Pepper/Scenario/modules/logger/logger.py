#!/usr/bin/env python2
# -*- coding: utf-8 -*-
# Author: Adam Rojik

##### Imports
import os
import qi
import copy
import time



class writer(object):
   """Do not use directly, use createWriter instead. Supports types: FATAL, ERROR, WARNING, INFO, VERBOSE."""
   def __init__(self, config, name, fileLog = True, timed = False):
      self.levels = "FATAL, ERROR, WARNING, INFO, VERBOSE".split(", ")
      self.FATAL = self.levels.index("FATAL")
      self.ERROR = self.levels.index("ERROR")
      self.WARNING = self.levels.index("WARNING")
      self.INFO = self.levels.index("INFO")
      self.VERBOSE = self.levels.index("VERBOSE")
   
      self.logger = qi.Logger(name)
      
      self.timed = timed
      
      self.logFile = False
      if fileLog and config.exportPath != "":
         limiter = len(config.exportPath)
         if config.exportPath[-1] != "/":
            limiter = config.exportPath.rfind("/")
         if limiter != -1 and not os.path.exists(config.exportPath[0:limiter]):
            os.makedirs(config.exportPath[0:limiter])
            
         self.logFile = open(config.exportPath + name + ".txt", 'w')         


   def log(self, text, level):
      if level == self.FATAL:
         self.logger.fatal(text)
      elif level == self.ERROR:
         self.logger.error(text)
      elif level == self.WARNING:
         self.logger.warning(text)
      elif level == self.INFO:
         self.logger.info(text)
      elif level == self.VERBOSE:
         self.logger.verbose(text)
      
      if self.logFile:
         if self.timed:
            text = str(time.time()) + " "+ text
         self.logFile.write(text + "\n")
      

   def fatal(self, text):
      self.log(text, self.FATAL)
   def error(self, text):
      self.log(text, self.ERROR)
   def warning(self, text):
      self.log(text, self.WARNING)
   def info(self, text):
      self.log(text, self.INFO)
   def verbose(self, text):
      self.log(text, self.VERBOSE)


   def __exit__(self, exc_type, exc_value, traceback):
      if self.logFile:
         self.logFile.close()
         self.logFile = False



class logger(object):
   def __init__(self, exportPath = ""):
      """exportPath - path for outputs eg. 'recordings/001/SCENE-1/prefix'"""
      self.logger = qi.Logger("Logger")
      self.exportPath = exportPath
      self.time = time.time()
      self.writers = []
      
      
   def createWriter(self, name, fileLog = True, timed = False):
      """Creates instance of writer for writing logs."""
      if name in self.writers:
         self.logger.warning("Writer \""+name+"\" already exists.")
      else:
         self.logger.verbose("Writer \""+name+"\" being created.")
         self.writers.append(name)
         
      return writer(self, name, fileLog, timed)



if __name__ == "__main__":
   log = logger("recordings/te")
   test = log.createWriter("test", timed=True)
   test.info("Works!")