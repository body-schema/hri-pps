#!/usr/bin/env python2
# -*- coding: utf-8 -*-
# Author: Adam Rojik

from copy import deepcopy

tempMoves = {}
moves = {}

names = list()
times = list()
keys = list()

names.append("LAnklePitch")
times.append([0.88])
keys.append([[0.0106959, [3, -0.293333, 0], [3, 0, 0]]])

names.append("LAnkleRoll")
times.append([0.88])
keys.append([[-0.105804, [3, -0.293333, 0], [3, 0, 0]]])

names.append("LElbowRoll")
times.append([0.12])
keys.append([[-1.17635, [3, -0.04, 0], [3, 0, 0]]])

names.append("LElbowYaw")
times.append([0.12])
keys.append([[-0.682424, [3, -0.04, 0], [3, 0, 0]]])

names.append("LHand")
times.append([0.12])
keys.append([[0.2936, [3, -0.04, 0], [3, 0, 0]]])

names.append("LHipPitch")
times.append([0.88])
keys.append([[0.444902, [3, -0.293333, 0], [3, 0, 0]]])

names.append("LHipRoll")
times.append([0.88])
keys.append([[0.1335, [3, -0.293333, 0], [3, 0, 0]]])

names.append("LHipYawPitch")
times.append([0.88])
keys.append([[-0.136484, [3, -0.293333, 0], [3, 0, 0]]])

names.append("LKneePitch")
times.append([0.88])
keys.append([[-0.0890141, [3, -0.293333, 0], [3, 0, 0]]])

names.append("LShoulderPitch")
times.append([0.12])
keys.append([[1.10137, [3, -0.04, 0], [3, 0, 0]]])

names.append("LShoulderRoll")
times.append([0.12])
keys.append([[0.0444441, [3, -0.04, 0], [3, 0, 0]]])

names.append("LWristYaw")
times.append([0.12])
keys.append([[0.523599, [3, -0.04, 0], [3, 0, 0]]])

names.append("RAnklePitch")
times.append([0.88])
keys.append([[-0.00302602, [3, -0.293333, 0], [3, 0, 0]]])

names.append("RAnkleRoll")
times.append([0.88])
keys.append([[0.12583, [3, -0.293333, 0], [3, 0, 0]]])

names.append("RElbowRoll")
times.append([0.12])
keys.append([[0.253151, [3, -0.04, 0], [3, 0, 0]]])

names.append("RElbowYaw")
times.append([0.12])
keys.append([[0.789967, [3, -0.04, 0], [3, 0, 0]]])

names.append("RHand")
times.append([0.12])
keys.append([[0.2924, [3, -0.04, 0], [3, 0, 0]]])

names.append("RHipPitch")
times.append([0.88])
keys.append([[0.472429, [3, -0.293333, 0], [3, 0, 0]]])

names.append("RHipRoll")
times.append([0.88])
keys.append([[-0.11961, [3, -0.293333, 0], [3, 0, 0]]])

names.append("RHipYawPitch")
times.append([0.88])
keys.append([[-0.136484, [3, -0.293333, 0], [3, 0, 0]]])

names.append("RKneePitch")
times.append([0.88])
keys.append([[-0.0889301, [3, -0.293333, 0], [3, 0, 0]]])

names.append("RShoulderPitch")
times.append([0.12])
keys.append([[1.97737, [3, -0.04, 0], [3, 0, 0]]])

names.append("RShoulderRoll")
times.append([0.12])
keys.append([[-0.251617, [3, -0.04, 0], [3, 0, 0]]])

names.append("RWristYaw")
times.append([0.12])
keys.append([[-0.748635, [3, -0.04, 0], [3, 0, 0]]])

tempMoves['startlingARF'] = {}
tempMoves['startlingARF']['names'] = names
tempMoves['startlingARF']['times'] = times
tempMoves['startlingARF']['keys'] = keys
tempMoves['startlingARF']['timeout'] = 0


names = list()
times = list()
keys = list()

names.append("LAnklePitch")
times.append([0.8])
keys.append([[0.0106959, [3, -0.266667, 0], [3, 0, 0]]])

names.append("LAnkleRoll")
times.append([0.8])
keys.append([[-0.105804, [3, -0.266667, 0], [3, 0, 0]]])

names.append("LElbowRoll")
times.append([0.44])
keys.append([[-1.26551, [3, -0.146667, 0], [3, 0, 0]]])

names.append("LElbowYaw")
times.append([0.44])
keys.append([[-0.621311, [3, -0.146667, 0], [3, 0, 0]]])

names.append("LHand")
times.append([0.44])
keys.append([[0.2808, [3, -0.146667, 0], [3, 0, 0]]])

names.append("LHipPitch")
times.append([0.8])
keys.append([[0.444902, [3, -0.266667, 0], [3, 0, 0]]])

names.append("LHipRoll")
times.append([0.8])
keys.append([[0.1335, [3, -0.266667, 0], [3, 0, 0]]])

names.append("LHipYawPitch")
times.append([0.8])
keys.append([[-0.136484, [3, -0.266667, 0], [3, 0, 0]]])

names.append("LKneePitch")
times.append([0.8])
keys.append([[-0.0890141, [3, -0.266667, 0], [3, 0, 0]]])

names.append("LShoulderPitch")
times.append([0.44])
keys.append([[0.898883, [3, -0.146667, 0], [3, 0, 0]]])

names.append("LShoulderRoll")
times.append([0.44])
keys.append([[0.00916195, [3, -0.146667, 0], [3, 0, 0]]])

names.append("LWristYaw")
times.append([0.44])
keys.append([[0.912689, [3, -0.146667, 0], [3, 0, 0]]])

names.append("RAnklePitch")
times.append([0.8])
keys.append([[-0.00302602, [3, -0.266667, 0], [3, 0, 0]]])

names.append("RAnkleRoll")
times.append([0.8])
keys.append([[0.12583, [3, -0.266667, 0], [3, 0, 0]]])

names.append("RElbowRoll")
times.append([0.44])
keys.append([[1.13367, [3, -0.146667, 0], [3, 0, 0]]])

names.append("RElbowYaw")
times.append([0.44])
keys.append([[0.515382, [3, -0.146667, 0], [3, 0, 0]]])

names.append("RHand")
times.append([0.44])
keys.append([[0.2836, [3, -0.146667, 0], [3, 0, 0]]])

names.append("RHipPitch")
times.append([0.8])
keys.append([[0.472429, [3, -0.266667, 0], [3, 0, 0]]])

names.append("RHipRoll")
times.append([0.8])
keys.append([[-0.11961, [3, -0.266667, 0], [3, 0, 0]]])

names.append("RHipYawPitch")
times.append([0.8])
keys.append([[-0.136484, [3, -0.266667, 0], [3, 0, 0]]])

names.append("RKneePitch")
times.append([0.8])
keys.append([[-0.0889301, [3, -0.266667, 0], [3, 0, 0]]])

names.append("RShoulderPitch")
times.append([0.44])
keys.append([[0.0291878, [3, -0.146667, 0], [3, 0, 0]]])

names.append("RShoulderRoll")
times.append([0.44])
keys.append([[0.190175, [3, -0.146667, 0], [3, 0, 0]]])

names.append("RWristYaw")
times.append([0.44])
keys.append([[-1.7979, [3, -0.146667, 0], [3, 0, 0]]])

tempMoves['startlingBRF'] = {}
tempMoves['startlingBRF']['names'] = names
tempMoves['startlingBRF']['times'] = times
tempMoves['startlingBRF']['keys'] = keys
tempMoves['startlingBRF']['timeout'] = 1



names = list()
times = list()
keys = list()

names.append("LAnklePitch")
times.append([1.16])
keys.append([[0.0106959, [3, -0.386667, 0], [3, 0, 0]]])

names.append("LAnkleRoll")
times.append([1.16])
keys.append([[-0.105804, [3, -0.386667, 0], [3, 0, 0]]])

names.append("LElbowRoll")
times.append([0.52])
keys.append([[-1.48231, [3, -0.173333, 0], [3, 0, 0]]])

names.append("LElbowYaw")
times.append([0.52])
keys.append([[-0.720365, [3, -0.173333, 0], [3, 0, 0]]])

names.append("LHand")
times.append([0.52])
keys.append([[0.0304, [3, -0.173333, 0], [3, 0, 0]]])

names.append("LHipPitch")
times.append([1.16])
keys.append([[0.444902, [3, -0.386667, 0], [3, 0, 0]]])

names.append("LHipRoll")
times.append([1.16])
keys.append([[0.1335, [3, -0.386667, 0], [3, 0, 0]]])

names.append("LHipYawPitch")
times.append([1.16])
keys.append([[-0.136484, [3, -0.386667, 0], [3, 0, 0]]])

names.append("LKneePitch")
times.append([1.16])
keys.append([[-0.0890141, [3, -0.386667, 0], [3, 0, 0]]])

names.append("LShoulderPitch")
times.append([0.52])
keys.append([[0.66185, [3, -0.173333, 0], [3, 0, 0]]])

names.append("LShoulderRoll")
times.append([0.52])
keys.append([[-0.0330164, [3, -0.173333, 0], [3, 0, 0]]])

names.append("LWristYaw")
times.append([0.52])
keys.append([[-0.950159, [3, -0.173333, 0], [3, 0, 0]]])

names.append("RAnklePitch")
times.append([1.16])
keys.append([[-0.00302602, [3, -0.386667, 0], [3, 0, 0]]])

names.append("RAnkleRoll")
times.append([1.16])
keys.append([[0.12583, [3, -0.386667, 0], [3, 0, 0]]])

names.append("RElbowRoll")
times.append([0.52])
keys.append([[1.44851, [3, -0.173333, 0], [3, 0, 0]]])

names.append("RElbowYaw")
times.append([0.52])
keys.append([[1.06909, [3, -0.173333, 0], [3, 0, 0]]])

names.append("RHand")
times.append([0.52])
keys.append([[0.0248001, [3, -0.173333, 0], [3, 0, 0]]])

names.append("RHipPitch")
times.append([1.16])
keys.append([[0.472429, [3, -0.386667, 0], [3, 0, 0]]])

names.append("RHipRoll")
times.append([1.16])
keys.append([[-0.11961, [3, -0.386667, 0], [3, 0, 0]]])

names.append("RHipYawPitch")
times.append([1.16])
keys.append([[-0.136484, [3, -0.386667, 0], [3, 0, 0]]])

names.append("RKneePitch")
times.append([1.16])
keys.append([[-0.0889301, [3, -0.386667, 0], [3, 0, 0]]])

names.append("RShoulderPitch")
times.append([0.52])
keys.append([[0.171438, [3, -0.173333, 0], [3, 0, 0]]])

names.append("RShoulderRoll")
times.append([0.52])
keys.append([[0.207794, [3, -0.173333, 0], [3, 0, 0]]])

names.append("RWristYaw")
times.append([0.52])
keys.append([[1.00099, [3, -0.173333, 0], [3, 0, 0]]])

tempMoves['startlingCRF'] = {}
tempMoves['startlingCRF']['names'] = names
tempMoves['startlingCRF']['times'] = times
tempMoves['startlingCRF']['keys'] = keys
tempMoves['startlingCRF']['timeout'] = 1



names = list()
times = list()
keys = list()

names.append("LAnklePitch")
times.append([0.52])
keys.append([[0.0106959, [3, -0.173333, 0], [3, 0, 0]]])

names.append("LAnkleRoll")
times.append([0.52])
keys.append([[-0.105804, [3, -0.173333, 0], [3, 0, 0]]])

names.append("LElbowRoll")
times.append([0.12])
keys.append([[-1.12131, [3, -0.04, 0], [3, 0, 0]]])

names.append("LElbowYaw")
times.append([0.12])
keys.append([[-0.659662, [3, -0.04, 0], [3, 0, 0]]])

names.append("LHand")
times.append([0.12])
keys.append([[0.2928, [3, -0.04, 0], [3, 0, 0]]])

names.append("LHipPitch")
times.append([0.52])
keys.append([[0.444902, [3, -0.173333, 0], [3, 0, 0]]])

names.append("LHipRoll")
times.append([0.52])
keys.append([[0.1335, [3, -0.173333, 0], [3, 0, 0]]])

names.append("LHipYawPitch")
times.append([0.52])
keys.append([[-0.136484, [3, -0.173333, 0], [3, 0, 0]]])

names.append("LKneePitch")
times.append([0.52])
keys.append([[-0.0890141, [3, -0.173333, 0], [3, 0, 0]]])

names.append("LShoulderPitch")
times.append([0.12])
keys.append([[1.12745, [3, -0.04, 0], [3, 0, 0]]])

names.append("LShoulderRoll")
times.append([0.12])
keys.append([[0.0889301, [3, -0.04, 0], [3, 0, 0]]])

names.append("LWristYaw")
times.append([0.12])
keys.append([[0.538392, [3, -0.04, 0], [3, 0, 0]]])

names.append("RAnklePitch")
times.append([0.52])
keys.append([[-0.00302602, [3, -0.173333, 0], [3, 0, 0]]])

names.append("RAnkleRoll")
times.append([0.52])
keys.append([[0.12583, [3, -0.173333, 0], [3, 0, 0]]])

names.append("RElbowRoll")
times.append([0.12])
keys.append([[1.15208, [3, -0.04, 0], [3, 0, 0]]])

names.append("RElbowYaw")
times.append([0.12])
keys.append([[0.773094, [3, -0.04, 0], [3, 0, 0]]])

names.append("RHand")
times.append([0.12])
keys.append([[0.2924, [3, -0.04, 0], [3, 0, 0]]])

names.append("RHipPitch")
times.append([0.52])
keys.append([[0.472429, [3, -0.173333, 0], [3, 0, 0]]])

names.append("RHipRoll")
times.append([0.52])
keys.append([[-0.11961, [3, -0.173333, 0], [3, 0, 0]]])

names.append("RHipYawPitch")
times.append([0.52])
keys.append([[-0.136484, [3, -0.173333, 0], [3, 0, 0]]])

names.append("RKneePitch")
times.append([0.52])
keys.append([[-0.0889301, [3, -0.173333, 0], [3, 0, 0]]])

names.append("RShoulderPitch")
times.append([0.12])
keys.append([[0.974132, [3, -0.04, 0], [3, 0, 0]]])

names.append("RShoulderRoll")
times.append([0.12])
keys.append([[-0.021518, [3, -0.04, 0], [3, 0, 0]]])

names.append("RWristYaw")
times.append([0.12])
keys.append([[-0.671934, [3, -0.04, 0], [3, 0, 0]]])

tempMoves['startlingDRF'] = {}
tempMoves['startlingDRF']['names'] = names
tempMoves['startlingDRF']['times'] = times
tempMoves['startlingDRF']['keys'] = keys
tempMoves['startlingDRF']['timeout'] = 1


# naming - startling[ID A|B|C][Right|Left avoiding hand][other hand does Foreward|Nothing (generated)]

switchAvoidingHand = {"R":"L", "L":"R"}

handJoints = "ElbowRoll,ElbowYaw,Hand,ShoulderPitch,ShoulderRoll,WristYaw".split(",")

for moveKey in tempMoves:
   move = tempMoves[moveKey]
   avoidingHand = moveKey[-2]
   newMoveKey = moveKey[:-2] + switchAvoidingHand[avoidingHand] + moveKey[-1]
   
   moves[moveKey] = deepcopy(move)
   
   newMoves = []
   for mvIndex,mvName in enumerate(move['names']):
      mvNewName = mvName
      if mvName[0] in switchAvoidingHand and mvName[1:] in handJoints:
         mvNewName = switchAvoidingHand[mvName[0]] + mvName[1:]
      newMoves.append(mvNewName)

   move['names'] = newMoves
   moves[newMoveKey] = deepcopy(move)

   for key in moveKey, newMoveKey:
      newKey = key[:-1] + "N"
      moves[newKey] = deepcopy(moves[key])
      
      newNames = []
      newTimes = []
      newKeys = []
      
      for mvIndex, mvName in enumerate(moves[newKey]['names']):
         if not (mvName[0] == newMoveKey[-2] and mvName[1:] in handJoints):
            newNames.append(mvName)
            newTimes.append(moves[newKey]['times'][mvIndex])
            newKeys.append(moves[newKey]['keys'][mvIndex])
            
      moves[newKey]['names'] = newNames
      moves[newKey]['times'] = newTimes
      moves[newKey]['keys'] = newKeys
      
print(moves)