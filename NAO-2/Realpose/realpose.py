from __future__ import print_function
#!/usr/bin/env python2
# -*- coding: utf-8 -*-
# Author: Adam Rojik

##### Imports
import sys, os
import argparse
import numpy as np
import datetime as dt
import time
import math
import json
from collections import deque

import videoPose as vid
import movementNAO21 as movement
import yarpConnectorSimple as skinRead


### Robot
# camera calibration
toRobRotation = np.eye(3)
toRobTranslation = np.array([0, 0, 0])
# connection
robotIP = "smurf.local"
robotPort = 9559

##### Program
isDebug = False
isView = True
startup = True
isRecording = False
recordDir = "unspecified"

## DEFAULT SCENE
SCENE = 6

allowedScenes = [0, 1, 2, 3, 7, 6, 13, 12]
sceneSequence = list(allowedScenes)

parser = argparse.ArgumentParser(description='Robot scenarios implementation.')
parser.add_argument('-d', '--debug', type=bool, default=False,
                    help='activate debug mode')
parser.add_argument('-x', '--xset', type=int,
                    help='load X-th scene from sequence based on ID (starting from 1) '+str(sceneSequence))
parser.add_argument('-u', '--user', type=int,
                    help='set persons ID (sets output name, generates sequence)')
parser.add_argument('-s', '--scene', type=int,
                    help='load scene (overwrites load from set, 0 = Familiarisation, 1 = CONTROL, 2 = LEAN BACK + NAO LIMITS, 3 = LEAN BACK + HUMAN LIMITS, 4 = TOUCH + HUMAN LIMITS, 5 = TOUCH + NO REACTION, 6 = startlingAXF, 7 = startlingAXN, 8 = startlingBXF, 9 = startlingBXN, 10 = startlingCXF, 11 = startlingCXN, 12 = startlingDXF, 13 = startlingDXN)')
parser.add_argument('-o', '--output', type=str,
                    help='record scenario to given output folder in "recordings/", will not record by default (eg. "0123/")')
parser.add_argument('-c', '--calibration', type=str,
                    help='use exported calibration settings ("'+json.dumps([toRobRotation.tolist(), toRobTranslation.tolist()])+'")')
parser.add_argument('-i', '--ip', type=str, default = robotIP,
                    help='NAOs IP address, get from "ping smurf.local"')
parser.add_argument('-p', '--port', type=int, default = robotPort,
                    help='NAOs port, should not change, but from Choregraphe')

args = parser.parse_args()
isDebug = args.debug
robotIP = args.ip
robotPort = args.port


if not args.user is None:
   sceneSequence = [0]
   a = [1+(2+args.user)%3, 1+(args.user)%3, 1+(1+args.user)%3]
   b = [7,6,13,12]
   for i in range((args.user-1)%4):
      temp = b[0]
      del b[0]
      b.append(temp)
      
   if args.user%2 == 1:
      sceneSequence = a+b
   else:
      sceneSequence = b+a
   
   sceneSequence = [0]+sceneSequence
   args.output = str(args.user)
   print("Setting scene sequence "+str(sceneSequence)+" and output to "+str(args.output))


if not args.xset is None:
   if not args.xset in range(1, len(sceneSequence)+1):
      print("Wrong set number")
      sys.exit(0)
   else:
      SCENE = sceneSequence[args.xset - 1]
      print("Setting "+str(args.xset)+"-nth scene from sequence.")


if not args.scene is None:
   if not args.scene in allowedScenes:
      print("Wrong scene "+str(args.scene))
      sys.exit(0)
   else:
      SCENE = args.scene
      print("Setting "+str(SCENE)+"-nth scene")


if not args.calibration is None:
   try:
      calibration = json.loads(args.calibration)
      toRobRotation = np.array(calibration[0])
      toRobTranslation = np.array(calibration[1])
      toRobRotation.shape = (3,3)
      
      startup = False
      print("Imported calibration.")
   except:
      print("Unable to process calibration string, ignoring argument.")


if not args.output is None:
   recordDir = args.output
   isRecording = True
   print('Recording output to folder: "'+recordDir+'".')



#### Scene set
# 1 = CONTROL
# 2 = LEANBACK + NAO LIMITS
# 3 = LEANBACK + HUMAN LIMITS
# 4 = TOUCH + HUMAN LIMITS
# 5 = TOUCH + NO REACTION
print("Running SCENE", SCENE)
selectedZone = 'HUMAN'
if SCENE in [2]:
   selectedZone = 'NAO'

startlingName = 'default'
startlingIDs = ['AXF', 'AXN', 'BXF', 'BXN', 'CXF', 'CXN', 'DXF', 'DXN']

allowLeanback = False
allowTouch = True
allowHeadTracking = False
allowHeadTrackingStopOnLeave = True
allowHeadTrackingStartOnTouch = True
allowLeanbackStopOnLeave = True
allowLeanbackStartOnTouch = True

if SCENE in range(1,6): # 1-5
   allowLeanback = True
   allowTouch = False
   allowHeadTracking = True
   allowHeadTrackingStopOnLeave = False
   allowHeadTrackingStartOnTouch = False
   allowLeanbackStopOnLeave = False
   allowLeanbackStartOnTouch = False

if SCENE in [5]:
   allowLeanback = False
   allowHeadTracking = False

defaultLookat = [10, .01, .01]

if SCENE in range(6,14): # 6-13
   startlingName = startlingIDs[SCENE-6]
   print("Doing startling type: "+startlingName)
   defaultLookat = [10, 10, .2]

startlingLists = {
      'default': ['startlingleft', 'startlingcenter', 'startlingright'],

      'AXF': ['startlingALF', 'startlingAMF', 'startlingARF'],
      'AXN': ['startlingALN', 'startlingAMN', 'startlingARN'],
      
      'BXF': ['startlingBLF', 'startlingBMF', 'startlingBRF'],
      'BXN': ['startlingBLN', 'startlingBMN', 'startlingBRN'],
      
      'CXF': ['startlingCLF', 'startlingCMF', 'startlingCRF'],
      'CXN': ['startlingCLN', 'startlingCMN', 'startlingCRN'],
      
      'DXF': ['startlingDLF', 'startlingDMF', 'startlingDRF'],
      'DXN': ['startlingDLN', 'startlingDMN', 'startlingDRN'],
}
startlingList = startlingLists[startlingName]

behaviorsLoop = ["leanback", "startlingARN", "startlingARF", "startlingDRN", "startlingDRF"]

allowWave = False # Disabled for being too expressive
print("ZONE:", selectedZone, "Leanback", allowLeanback, "Touch:", allowTouch, "Wave:", allowWave)

windowTreshold = .5

# Zones
ZONES = { # in meters (intimate, personal, social)
      'NAO': [0.16, 0.42, 1.27],
      'HUMAN': [0.45, 1.2, 3.7]
}
ZONE = ZONES[selectedZone]

# ROBOT
ROBOT = {
   'vision': {},
   'touch': {
      'side': None
   }
}

if isRecording:
   outputFolder = "recordings/" + recordDir
   if not os.path.exists(outputFolder):
      os.mkdir(outputFolder)
   print('Recording into "'+recordDir+'" folder')
   exportName = outputFolder + "/" + time.strftime("%y-%m-%d_%H-%M-%S")+"_SCENE-"+str(SCENE)
else:
   exportName = ""
startTime = time.time()


# Movements
mvConfig = movement.config()
if not mvConfig.connect(robotIP, robotPort):
   print("Robot not connected.")
else:
   mvConfig.setupSafeMotion(robotIP, robotPort)
mvConfig.isDebug = isDebug
mvConfig.isRecording = isRecording
mvConfig.exportName = exportName
mvConfig.startTime = startTime
mv = movement.start(mvConfig)


skinConfig = skinRead.skinConfig()
skinConfig.isDebug = isDebug
skinConfig.isRecording = isRecording
skinConfig.exportName = exportName
skinConfig.startTime = startTime
skinConfig.status = allowTouch

skin = skinRead.start(skinConfig)


vidConfig = vid.videoConfig()
vidConfig.startup = startup
vidConfig.ZONE = ZONE
vidConfig.SCENE = SCENE
vidConfig.motion = mv
vidConfig.isView = isView
vidConfig.isDebug = isDebug
vidConfig.isRecording = isRecording
vidConfig.exportName = exportName
vidConfig.startTime = startTime
vidConfig.skin = skin
if not startup:
   vidConfig.toRobRotation = toRobRotation
   vidConfig.toRobTranslation = toRobTranslation

video = vid.start(vidConfig)

np.random.seed()
randomMoveID = np.random.random_integers(0, len(behaviorsLoop))
rnNextTime = [time.time()]*2
rnNextTime[1] = time.time() + 20.
prevZone = 4
requestedMove = 'standnohead'
lastTodoLookat = None
lastTodoMove = None

totalWindowTime = 0
lastTimestep = 0
zonesWindow = deque([])
timesWindow = deque([])

try:
   while video.isRunning():
      # Load current senses
      ROBOT['touch']['side'] = skin.whichSide()
      ROBOT['vision'] = video.getVision()
      
      # Vision processing
      if ROBOT['touch']['side'] != None:
         ROBOT['vision']['zone']['current'] = 0

      ROBOT['vision']['zone']['velocity'] = prevZone - ROBOT['vision']['zone']['current']
      prevZone = ROBOT['vision']['zone']['current']
      
      # Zones smoothing
      t = time.time()
      if lastTimestep != 0:
         tdiff = t - lastTimestep

         while totalWindowTime - tdiff > windowTreshold:
            totalWindowTime -= timesWindow.pop()
            zonesWindow.pop()

         totalWindowTime += tdiff
         timesWindow.appendleft(tdiff)
         zonesWindow.appendleft(ROBOT['vision']['zone']['current'])
      lastTimestep = t

      # ROBOT reactions
      lastMove = mv.lastMove() # do not use directly, locks variable

      todoLookat = None
      todoMove = None
      
      ### Familiarisation
      if SCENE == 0:
         if t > rnNextTime[0]:
            rnNextTime[0] = time.time() + np.random.normal(loc=5.0, scale=1.0, size=None)
            x = np.random.random()*4-2
            y = np.random.random()*2
            todoLookat = [3, x, y]
         
         if t > rnNextTime[1]:
            if lastMove == 'standnohead':
               rnNextTime[1] = time.time() + 5.
               requestedMove = behaviorsLoop[randomMoveID%len(behaviorsLoop)]
               randomMoveID += 1
            else:
               rnNextTime[1] = time.time() + np.random.normal(loc=5.0, scale=1.0, size=None)
               requestedMove = 'standnohead'

         if requestedMove != lastMove:
            todoMove = requestedMove
            
      # Control
      if SCENE == 1:
         if t > rnNextTime[0]:
            rnNextTime[0] = time.time() + np.random.normal(loc=5.0, scale=1.0, size=None)
            x = np.random.random()*4-2
            y = np.random.random()*2
            todoLookat = [3, x, y]
         
         if t > rnNextTime[1]:
            if lastMove == 'standnohead':
               rnNextTime[1] = time.time() + 5.
               requestedMove = 'leanback'
            else:
               rnNextTime[1] = time.time() + 20.
               requestedMove = 'standnohead'

         if requestedMove != lastMove:
            todoMove = requestedMove
            
      if SCENE in (i for j in (range(2,5),range(6,14)) for i in j): # 2-4 + 6-13
         if ROBOT['vision']['zone']['velocity'] < 0 and ROBOT['vision']['zone']['current'] == 3 and lastMove != 'wave': # getting closer
            if allowWave:
               todoMove = 'wave'
               allowHeadTracking = False
            
         if ROBOT['vision']['zone']['current'] == 1: # intimate
            if allowLeanback:
               todoMove = "leanbackInterpolated"

         if ROBOT['vision']['zone']['current'] > 1:
            if lastMove != 'standnohead':
               todoMove = 'standnohead'
   
         if ROBOT['vision']['zone']['current'] == 0: # touch
            if allowTouch:
               if ROBOT['touch']['side'] == 1: # ROBOT['touch']['side'] = -1,0,1 (right)
                  todoLookat = ROBOT['vision']['objects']['nose']['coords']
                  mv.lookat(todoLookat)
                  todoMove = startlingList[ROBOT['touch']['side']+1]

                  if allowHeadTrackingStartOnTouch:
                     allowHeadTracking = True
                  if allowLeanbackStartOnTouch:
                     allowLeanback = True

               if ROBOT['touch']['side'] == 0:
                  mv.lookat(defaultLookat)
         
         if ROBOT['vision']['zone']['current'] <= 2 and ROBOT['vision']['zone']['current'] > 0:
            if allowHeadTracking and ROBOT['vision']['objects']['nose']['current']:
               todoLookat = ROBOT['vision']['objects']['nose']['coords']
         elif ROBOT['vision']['zone']['current'] > 2:
            todoLookat = np.array(defaultLookat)
            if allowHeadTrackingStopOnLeave:
               allowHeadTracking = False
            if allowLeanbackStopOnLeave:
               allowLeanback = False
                  
      if SCENE == 5:
         pass

      if todoLookat is not None and (np.array(todoLookat) == np.array(lastTodoLookat)).sum() != 3:
         mv.lookat(todoLookat)
         lastTodoLookat = np.array(todoLookat)
      
      currentMove = mv.currentMove() # loading here, so it is the most current status
      
      # If touched - reaction must happen
      if lastTodoMove in startlingList and lastTodoMove != lastMove:
         todoMove = lastTodoMove

      if todoMove != None and ( currentMove in (None, todoMove) or todoMove in startlingList or (currentMove[:8] == 'leanback' and currentMove[:8] == todoMove[:8]) ):
         if todoMove == "leanbackInterpolated":
            ratio = np.clip((ROBOT['vision']['distance']['current']-ZONE[0]/6)/(ZONE[0]-ZONE[0]/6), 0., 1.)
            mv.doInterpolated('leanback('+str(ratio)+')', 'leanback', 'standnohead', ratio)
         else:
            mv.do(todoMove)

         lastTodoMove = todoMove
      
      time.sleep(.1)
finally:
   print("Stopping everything.")
   video.stop()
   mv.stop()
   skin.stop()
