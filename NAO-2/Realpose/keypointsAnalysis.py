from __future__ import print_function
#!/usr/bin/env python2
# -*- coding: utf-8 -*-
# Author: Adam Rojik
import numpy as np
import importlib
from pathlib import Path, PosixPath
import sys,os
from matplotlib import pyplot as plt, rcParams
import json


for i in range(1,4):
   distances = []
   resolution = .01

   for filePath in Path('.').glob('recordings/*/*'+str(i)+'_keypoints.py'):
      filePathSetup = PosixPath(str(filePath)[:-len('_keypoints.py')]+'_setup.txt')
      setupArg = Path(filePathSetup).read_text()
      jsonDataStart = setupArg.index('"')
      calibration = json.loads(setupArg[jsonDataStart+1:-1])
      toRobRotation = np.array(calibration[0])
      toRobTranslation = np.array(calibration[1])
      toRobRotation.shape = (3,3)
      
      exec(Path(filePath).read_text())
      for keypointMesh in keypoints3D:
         if keypointMesh != {}:
            distance = None
            for keypoint in keypointMesh:
               distanceUpdate = np.linalg.norm(np.dot(toRobRotation, np.array(keypointMesh[keypoint])) + toRobTranslation - np.array([0.05871,0.0,0.06364]))
               if distanceUpdate < .5 and (distance == None or distanceUpdate < distance):
                  distance = distanceUpdate
         
            if distance != None:
               distances.append(distance)

   distances = np.array(distances)
   plt.hist(distances, int(distances.max(axis=0)/resolution), alpha=.4, density=True)

plt.legend(["Control", "Robot", "Human"])

plt.xlabel("Distance [m]")
plt.ylabel("Probability density")

rcParams.update({'font.weight': 'bold'})

c = (0,28/255,168/255)
plt.axvline(.45, color=c)
#plt.axvline(1.2, color=c, linestyle="dashed")
labels = ["intimate (human scale)", "personal (human scale)"]
plt.text(.45+.03, .07, labels[0], color=c, ha='right', va='bottom', rotation='vertical')
#plt.text(1.2-.07, 4.2, labels[1], color=c, ha='left', va='top', rotation='vertical')

c = (65/255,118/255,48/255)
plt.axvline(.16, color=c)
plt.axvline(.42, color=c, linestyle="dashed")
labels = ["intimate (robot scale)", "personal (robot scale)"]
plt.text(.16-.03, 4.2, labels[0], color=c, ha='left', va='top', rotation='vertical')
plt.text(.42-.03, .07, labels[1], color=c, ha='left', va='bottom', rotation='vertical')

plt.savefig('DistanceDistribution.png', format='png', dpi=900)
plt.show()