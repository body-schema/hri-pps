<?xml version="1.0" encoding="UTF-8" ?>
<Package name="ProxemicsBehaviors" format_version="4">
    <Manifest src="manifest.xml" />
    <BehaviorDescriptions>
        <BehaviorDescription name="behavior" src="behavior_1" xar="behavior.xar" />
    </BehaviorDescriptions>
    <Dialogs />
    <Resources>
        <File name="choice_sentences" src="behavior_1/Aldebaran/choice_sentences.xml" />
        <File name="_metadata" src="_metadata" />
        <File name="translation_en_US" src="translations/translation_en_US.ts" />
    </Resources>
    <Topics />
    <IgnoredPaths />
</Package>
