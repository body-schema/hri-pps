# !/usr/bin/env python2
# -*- coding: utf-8 -*-
# Author: Adam Rojik
import pandas as pd
import matplotlib.pyplot as plt
from pylab import *
from pathlib import Path


class Outputs(object):
    def __init__(self):
        Path('output/histogram').mkdir(parents=True, exist_ok=True)
        Path('output/xy').mkdir(parents=True, exist_ok=True)
        self.conditions = [None, 1, 2, 3, [1, 2], [2, 3], [3, 1]]
        self.orders = [None, 1, 2, 3]
        self.robots = [None, "NAO-2", "Pepper"]
        self.events = ["FirstStop", "PickingUpTheBrick", "BrickPlaced", "Whispering", "Leanback"]
        self.measurements = ["Time", "Distance"]

        self.histogram_config = {
            "bins": 40,
            "robot": {
                "NAO-2": {
                    "color": "green",
                    "subactions": {
                        "condition": {
                            0: {"color": "lightgreen"},
                            1: {"color": "darkgreen"},
                            2: {"color": "teal"},
                            3: {"color": "dodgerblue"},
                        }
                    }
                },
                "Pepper": {
                    "color": "blue",
                    "subactions": {
                        "condition": {
                            0: {"color": "maroon"},
                            1: {"color": "salmon"},
                            2: {"color": "gold"},
                            3: {"color": "fuchsia"},
                        }
                    }
                }

            },
            "order": {
                1: {"alpha": .7},
                2: {"alpha": .5},
                3: {"alpha": .3},
            },
            "condition": {
                0: {"color": "silver"},
                1: {"color": "firebrick"},
                2: {"color": "yellowgreen"},
                3: {"color": "cornflowerblue"}
            }
        }
        self.palettes = {
            "NAO-2": {
                0: "lightgreen",
                1: "darkgreen",
                2: "teal",
                3: "dodgerblue",
                "label": "NAO-2"
            },
            "Pepper": {
                0: "maroon",
                1: "salmon",
                2: "gold",
                3: "fuchsia",
                "label": "Pepper"
            }
        }
        self.condition_data = {
            0: {
                "label": "Familiarization"
            },
            1: {
                "label": "Control"
            },
            2: {
                "label": "Robot"
            },
            3: {
                "label": "Human"
            }
        }
        self.measurement_data = {
            "Time": {
                "label": "Time [s]",
                "lim": [-.3, 82]
            },
            "Distance": {
                "label": "Distance [m]",
                "lim": [-0, 3.8]
            }
        }
        self.event_data = {
            "FirstStop": {
                "label": "First stop",
                "marker": "s"
            },
            "PickingUpTheBrick": {
                "label": "Picking up the brick",
                "marker": "^"
            },
            "BrickPlaced": {
                "label": "Brick placed",
                "marker": "v"
            },
            "Whispering": {
                "label": "Whispering",
                "marker": "X"
            },
            "Leanback": {
                "label": "Lean-back",
                "marker": "o"
            },
            "Stop": {
                "label": "Familiarization",
                "marker": "."
            }
        }
        self.edge_colors = {
            0: "silver",
            1: "orange",
            2: "green",
            3: "navy"
        }

        self.data = None
        self.init_data()

    def init_data(self):
        files = [
            {
                "robot": "NAO-2",
                "file": "NAO-2.xlsx",
                "sheet": "Second measurements"
            },
            {
                "robot": "Pepper",
                "file": "Pepper.xlsx",
                "sheet": "Measurements"
            }
        ]
        data = {
            "id": [],
            "condition_1": [],
            "condition_2": [],
            "condition_3": [],
            "order": [],
            "condition": [],
            "robot": [],
            "event": [],
            "measurement": [],
            "unit": [],
            "value": []
        }
        for file in files:
            file_data = pd.read_excel(file["file"], file["sheet"], index_col=0)
            next_splitter = ""
            for index, row in file_data.iterrows():
                for column in file_data.columns:
                    split_column = column.split(".")
                    if len(split_column) != 3:
                        continue
                    str_index = str(index)
                    data["robot"].append(file["robot"])
                    data["id"].append(int(str_index[:-7]))
                    if str_index[-7] in ['1', '2', '3']:
                        data["condition_1"].append(int(str_index[-7]))
                        data["condition_2"].append(int(str_index[-6]))
                        data["condition_3"].append(int(str_index[-5]))
                    else:
                        data["condition_1"].append(int(str_index[-3]))
                        data["condition_2"].append(int(str_index[-2]))
                        data["condition_3"].append(int(str_index[-1]))
                    data["event"].append(split_column[1])
                    measurements = {
                        "GazeDistance": "m",
                        "Distance": "m",
                        "Time": "s"
                    }
                    data["measurement"].append(split_column[2])
                    data["unit"].append(measurements[split_column[2]])
                    conditions = {
                        "F": 0,
                        "C": 1,
                        "R": 2,
                        "H": 3
                    }
                    data["condition"].append(conditions[split_column[0]])
                    data["order"].append(0)
                    for condition_order in range(1, 4):
                        if data["condition_" + str(condition_order)][-1] == data["condition"][-1]:
                            data["order"][-1] = condition_order
                            break
                    value = None
                    if row[column] != '-':
                        value = row[column]
                        if split_column[2] == "Time":
                            value = pd.to_timedelta(pd.to_datetime(value, format="%M:%S").strftime('00:%M:%S')) \
                                .total_seconds()
                            if split_column[1] == "FirstStop":
                                next_splitter = value
                            value -= next_splitter
                        else:
                            value = float(value)
                    data["value"].append(value)
        self.data = pd.DataFrame(data)
        self.measurement_data["Time"]["lim"][1] = self.data.query("measurement=='Time'")["value"].dropna().max() + 0.5
        self.measurement_data["Distance"]["lim"][1] = self.data.query("measurement=='Distance'")[
                                                          "value"].dropna().max() + 0.1

    def get_data(self, filters):
        query = '(value != None) and (not value.isnull()) and (event != "Stop") and '
        for data_filter in filters:
            filter_value = filters[data_filter]
            if filters[data_filter] is not None:
                query += "("
                filter_type = type(filter_value)
                if type(filter_value) is list:
                    filter_type = type(filter_value[0])
                s = '" or ' + data_filter + ' == "'
                if filter_type is not str:
                    s = s[1:-1]
                if type(filter_value) is list:
                    filter_value = s.join(map(str, filter_value))
                query += s[5 - int(filter_type is not str):] + str(filter_value)
                if filter_type is str:
                    query += '"'
                query += ') and '
        if query == "":
            return self.data
        #print(query[:-5])
        return self.data.query(query[:-5])

    def count_participants(self, data):
        p = data[data.robot == 'Pepper']
        n = data[data.robot == 'NAO-2']
        return p.id.unique().shape[0] + n.id.unique().shape[0]

    def generate_title(self, condition, order, robot, event, static_actions=None):
        title = ""
        if condition is not None:
            title += "Condition " + str(condition) + " "
        if order is not None:
            title += "Order " + str(order) + " "
        if robot is not None:
            title += "Robot " + str(robot) + " "
        if event is not None:
            title += "Event " + str(event) + " "
        if static_actions is not None:
            title += "(" + str(static_actions) + ") "
        return title

    def histogram(self, condition, order, robot, measurement, event, static_actions):
        data = self.get_data({
            "condition": condition,
            "order": order,
            "robot": robot,
            "event": event,
            "measurement": measurement
        })
        #print(self.count_participants(data))

        ax = plt.figure().gca()
        ax.yaxis.set_major_locator(MaxNLocator(integer=True))

        legend_actions = []
        #print(data)
        static_action = static_actions
        static_subaction = None
        if type(static_actions) == list:
            static_action = static_actions[0]
            static_subaction = static_actions[1]

        unique_actions = data[static_action].unique()
        for action_id in unique_actions:
            if type(action_id) == str:
                action_data = data.query(f'{static_action}=="{action_id}"')
            else:
                action_data = data.query(f'{static_action}=={action_id}')

            alpha = .7
            color = "purple"
            if static_action in self.histogram_config and \
                    action_id in self.histogram_config[static_action]:
                palette = self.histogram_config[static_action][action_id]
                if "color" in palette:
                    color = palette["color"]
                if "alpha" in palette:
                    alpha = palette["alpha"]

            if static_subaction is not None:
                unique_subactions = action_data[static_subaction].unique()
                for subaction_id in unique_subactions:
                    if type(subaction_id) == str:
                        subaction_data = action_data.query(f'{static_subaction}=="{subaction_id}"')
                    else:
                        subaction_data = action_data.query(f'{static_subaction}=={subaction_id}')

                    if static_subaction in self.histogram_config and \
                            subaction_id in self.histogram_config[static_subaction]:
                        subpalette = self.histogram_config[static_subaction][subaction_id]
                        if "color" in subpalette:
                            color = subpalette["color"]
                        if "alpha" in subpalette:
                            alpha = subpalette["alpha"]
                    if "subactions" in palette and static_subaction in palette["subactions"] and \
                        subaction_id in palette["subactions"][static_subaction]:
                        subpalette = palette["subactions"][static_subaction][subaction_id]
                        if "color" in subpalette:
                            color = subpalette["color"]
                        if "alpha" in subpalette:
                            alpha = subpalette["alpha"]
                    plt.hist(subaction_data["value"], self.histogram_config["bins"], alpha=alpha,
                             color=[color],
                             range=self.measurement_data[measurement]["lim"])
                    legend_actions.append(
                        Line2D([0], [0], marker='s', markersize=8, markerfacecolor=color, color='w', lw=4,
                               label=str(action_id) + " " + str(subaction_id) + " (" + str(self.count_participants(subaction_data)) + ")"
                               )
                    )
            else:
                plt.hist(action_data["value"], self.histogram_config["bins"], alpha=alpha,
                         color=[color],
                         range=self.measurement_data[measurement]["lim"])
                legend_actions.append(
                    Line2D([0], [0], marker='s', markersize=8, markerfacecolor=color, color='w', lw=4,
                           label=str(action_id)+" ("+str(self.count_participants(action_data))+")"
                           )
                )
        plt.legend(handles=legend_actions, loc="upper right", title=static_action + " (participants)",
                   framealpha=0.2)
        plt.xlabel(self.measurement_data[measurement]["label"])
        plt.ylabel("Count")
        # condition, order, robot, measurement, event, static_actions
        title = self.generate_title(condition, order, robot, event, static_actions)
        plt.title(title + " (" + str(self.count_participants(data)) + ")")

        plt.savefig('output/histogram/' + title.replace(" ", "-").lower() + "measurement-" + str(measurement).lower() +
                    '.png', format='png', dpi=1500)

    def xy_plot(self, condition, order, robot, measurement, event=None):
        data = self.get_data({
            "condition": condition,
            "order": order,
            "robot": robot,
            "event": event
        })

        fig = plt.figure()
        a1 = fig.add_subplot(111)

        x_selector = measurement
        y_selector = list(self.measurements)
        y_selector.remove(measurement)
        y_selector = y_selector[0]

        legend_events = []
        legend_conditions = []
        unique_robots = data["robot"].unique()
        for robot_id in unique_robots:
            palette = self.palettes[robot_id]
            robot_data = data.query(f'robot=="{robot_id}"')

            unique_conditions = robot_data["condition"].unique()
            for condition_id in unique_conditions:
                color = palette[condition_id]
                condition_data = robot_data.query(f'condition=={condition_id}')

                legend_conditions.append(
                    Line2D([0], [0], marker='s', markersize=8, markerfacecolor=color, color='w', lw=4,
                           label=palette["label"] + " " +
                                 self.condition_data[condition_id]["label"] + " (" + str(
                                 self.count_participants(condition_data)) + ")"
                           )
                )

                unique_events = condition_data["event"].unique()
                for event_id in unique_events:
                    marker = self.event_data[event_id]["marker"]
                    event_data = condition_data.query(f'event=="{event_id}"')

                    unique_orders = event_data["order"].unique()
                    for order_id in unique_orders:
                        alpha = 1#(4 - order_id) / 4
                        edge_color = self.edge_colors[order_id]
                        order_data = event_data.query(f'order=={order_id}')

                        x = order_data.query(f'measurement=="{x_selector}"')
                        y = order_data.query(f'measurement=="{y_selector}"')

                        a1.set_xlim(self.measurement_data[x_selector]["lim"][0],
                                    self.measurement_data[x_selector]["lim"][1])
                        a1.set_ylim(self.measurement_data[y_selector]["lim"][0],
                                    self.measurement_data[y_selector]["lim"][1])

                        a1.scatter(x["value"], y["value"], s=8, color=color, marker=marker, lw=.4, alpha=alpha,
                                   edgecolor=edge_color)

        unique_events = data["event"].unique()
        for event_id in unique_events:
            marker = self.event_data[event_id]["marker"]
            event_data = data.query(f'event=="{event_id}"')
            legend_events.append(Line2D(
                [0],
                [0],
                marker=marker,
                markersize=8,
                markerfacecolor='gray',
                color='w',
                lw=4,
                label=self.event_data[event_id]["label"] + " (" + str(self.count_participants(event_data)) + ")"
            ))

        legends = a1.legend(handles=legend_events, loc="upper right", title="Event (participants)", framealpha=0.2)
        a1.add_artist(legends)
        a1.legend(handles=legend_conditions, loc="lower right", title="ROBOT-Condition (participants)", framealpha=0.2)
        a1.set_xlabel(self.measurement_data[x_selector]["label"])
        a1.set_ylabel(self.measurement_data[y_selector]["label"])

        title = self.generate_title(condition, order, robot, event)
        a1.set_title(title + " (" + str(self.count_participants(data)) + ")")
        fig.set_dpi(1500)
        plt.savefig("output/xy/" + title.replace(" ", "-").lower() + "measurement-" + str(measurement).lower() + ".png")


o = Outputs()
for condition in o.conditions:
    for order in o.orders:
        for robot in o.robots:
            for measurement in o.measurements:
                for event in o.events:
                    for static_event in [["robot", "condition"], "order", "robot", "condition",
                                         ["robot", "condition"], ["robot", "order"], ["condition", "order"]]:
                        if event == "FirstStop" and measurement == "Time":
                            continue
                        o.histogram(condition, order, robot, measurement, event, static_event)
                    # o.xy_plot(condition, order, robot, measurement, event)
                    pass
                o.xy_plot(condition, order, robot, measurement)
