#!/usr/bin/env python2
# -*- coding: utf-8 -*-
# Author: Adam Rojik

from copy import deepcopy

tempMoves = {}
moves = {}

names = list()
times = list()
keys = list()

names.append("HipPitch")
times.append([0.28])
keys.append([[0.188681, [3, -0.106667, 0], [3, 0, 0]]])

names.append("HipRoll")
times.append([0.28])
keys.append([[-0.00613595, [3, -0.106667, 0], [3, 0, 0]]])

names.append("KneePitch")
times.append([0.28])
keys.append([[0.0107379, [3, -0.106667, 0], [3, 0, 0]]])

names.append("LElbowRoll")
times.append([0.24])
keys.append([[-1.17635, [3, -0.0933333, 0], [3, 0, 0]]])

names.append("LElbowYaw")
times.append([0.24])
keys.append([[-0.682424, [3, -0.0933333, 0], [3, 0, 0]]])

names.append("LHand")
times.append([0.24])
keys.append([[0.02, [3, -0.0933333, 0], [3, 0, 0]]])

names.append("LShoulderPitch")
times.append([0.24])
keys.append([[1.10137, [3, -0.0933333, 0], [3, 0, 0]]])

names.append("LShoulderRoll")
times.append([0.24])
keys.append([[0.0444441, [3, -0.0933333, 0], [3, 0, 0]]])

names.append("LWristYaw")
times.append([0.24])
keys.append([[0.523599, [3, -0.0933333, 0], [3, 0, 0]]])

names.append("RElbowRoll")
times.append([0.24])
keys.append([[0.253151, [3, -0.0933333, 0], [3, 0, 0]]])

names.append("RElbowYaw")
times.append([0.24])
keys.append([[0.789967, [3, -0.0933333, 0], [3, 0, 0]]])

names.append("RHand")
times.append([0.24])
keys.append([[0.2924, [3, -0.0933333, 0], [3, 0, 0]]])

names.append("RShoulderPitch")
times.append([0.24])
keys.append([[1.97737, [3, -0.0933333, 0], [3, 0, 0]]])

names.append("RShoulderRoll")
times.append([0.24])
keys.append([[-0.251617, [3, -0.0933333, 0], [3, 0, 0]]])

names.append("RWristYaw")
times.append([0.24])
keys.append([[-0.748635, [3, -0.0933333, 0], [3, 0, 0]]])

tempMoves['startlingARF'] = {}
tempMoves['startlingARF']['names'] = names
tempMoves['startlingARF']['times'] = times
tempMoves['startlingARF']['keys'] = keys
tempMoves['startlingARF']['timeout'] = 0


names = list()
times = list()
keys = list()

# Choregraphe bezier export in Python.
from naoqi import ALProxy
names = list()
times = list()
keys = list()

names.append("HipPitch")
times.append([0.28])
keys.append([[0.188681, [3, -0.106667, 0], [3, 0, 0]]])

names.append("HipRoll")
times.append([0.28])
keys.append([[-0.00613595, [3, -0.106667, 0], [3, 0, 0]]])

names.append("KneePitch")
times.append([0.28])
keys.append([[0.0107379, [3, -0.106667, 0], [3, 0, 0]]])

names.append("LElbowRoll")
times.append([0.24])
keys.append([[-1.12131, [3, -0.0933333, 0], [3, 0, 0]]])

names.append("LElbowYaw")
times.append([0.24])
keys.append([[-0.659661, [3, -0.0933333, 0], [3, 0, 0]]])

names.append("LHand")
times.append([0.24])
keys.append([[0.02, [3, -0.0933333, 0], [3, 0, 0]]])

names.append("LShoulderPitch")
times.append([0.24])
keys.append([[1.12745, [3, -0.0933333, 0], [3, 0, 0]]])

names.append("LShoulderRoll")
times.append([0.24])
keys.append([[0.0889301, [3, -0.0933333, 0], [3, 0, 0]]])

names.append("LWristYaw")
times.append([0.24])
keys.append([[0.538392, [3, -0.0933333, 0], [3, 0, 0]]])

names.append("RElbowRoll")
times.append([0.24])
keys.append([[1.15208, [3, -0.0933333, 0], [3, 0, 0]]])

names.append("RElbowYaw")
times.append([0.24])
keys.append([[0.773094, [3, -0.0933333, 0], [3, 0, 0]]])

names.append("RHand")
times.append([0.24])
keys.append([[0.02, [3, -0.0933333, 0], [3, 0, 0]]])

names.append("RShoulderPitch")
times.append([0.24])
keys.append([[0.974133, [3, -0.0933333, 0], [3, 0, 0]]])

names.append("RShoulderRoll")
times.append([0.24])
keys.append([[-0.021518, [3, -0.0933333, 0], [3, 0, 0]]])

names.append("RWristYaw")
times.append([0.24])
keys.append([[-0.671934, [3, -0.0933333, 0], [3, 0, 0]]])

tempMoves['startlingBRF'] = {}
tempMoves['startlingBRF']['names'] = names
tempMoves['startlingBRF']['times'] = times
tempMoves['startlingBRF']['keys'] = keys
tempMoves['startlingBRF']['timeout'] = 0


# naming - startling[ID A|B|..][Right|Left avoiding hand][other hand does Foreward|Nothing (generated)]

switchAvoidingHand = {"R":"L", "L":"R"}

handJoints = "ElbowRoll,ElbowYaw,Hand,ShoulderPitch,ShoulderRoll,WristYaw".split(",")
switchSigns = "ElbowRoll,ElbowYaw,ShoulderRoll,WristYaw".split(",")

for moveKey in tempMoves:
   move = tempMoves[moveKey]
   avoidingHand = moveKey[-2]
   newMoveKey = moveKey[:-2] + switchAvoidingHand[avoidingHand] + moveKey[-1]
   
   moves[moveKey] = deepcopy(move)
   
   newMoves = []
   for mvIndex,mvName in enumerate(move['names']):
      mvNewName = mvName
      if mvName[0] in switchAvoidingHand and mvName[1:] in handJoints:
         mvNewName = switchAvoidingHand[mvName[0]] + mvName[1:]
         if mvName[1:] in switchSigns:
            for k,_ in enumerate(move['keys'][mvIndex]):
               move['keys'][mvIndex][k][0] *= -1
      newMoves.append(mvNewName)

   move['names'] = newMoves
   moves[newMoveKey] = deepcopy(move)

   for key in moveKey, newMoveKey:
      newKey = key[:-1] + "N"
      moves[newKey] = deepcopy(moves[key])
      
      newNames = []
      newTimes = []
      newKeys = []
      
      for mvIndex, mvName in enumerate(moves[newKey]['names']):
         if not (mvName[0] == newMoveKey[-2] and mvName[1:] in handJoints):
            newNames.append(mvName)
            newTimes.append(moves[newKey]['times'][mvIndex])
            newKeys.append(moves[newKey]['keys'][mvIndex])
            
      moves[newKey]['names'] = newNames
      moves[newKey]['times'] = newTimes
      moves[newKey]['keys'] = newKeys
      
print(moves)