from __future__ import print_function
#!/usr/bin/env python2
# -*- coding: utf-8 -*-
# Author: Adam Rojik
import numpy as np
import importlib
from pathlib import Path, PosixPath
import sys,os,glob
from matplotlib import pyplot as plt, rcParams
import json
from matplotlib.ticker import MaxNLocator


INPUT = "/Volumes/HRI/HRI/2019_06-nao-interpersonalDistance/logsAndVideosFromExpsByParticipantID/"

ax = plt.figure().gca()
ax.yaxis.set_major_locator(MaxNLocator(integer=True))

colors = [None,(0.30,0.53,1.00),(0.20,1.00,0.20),(1.00,0.,0.00)]
for i in reversed(range(1,4)):
   distances = []
   personIDs = []
   resolution = .05

   for filePath in glob.glob(INPUT+'*/*'+str(i)+'_keypoints.py'):
      filePathSetup = PosixPath(str(filePath)[:-len('_keypoints.py')]+'_setup.txt')
      setupArg = Path(filePathSetup).read_text()
      jsonDataStart = setupArg.index('"')
      calibration = json.loads(setupArg[jsonDataStart+1:-1])
      toRobRotation = np.array(calibration[0])
      toRobTranslation = np.array(calibration[1])
      toRobRotation.shape = (3,3)
      
      personID = int(filePath[len(INPUT):].split("/")[0][:3])

      velocity = 0
      lastDistance = 0
      pDistances = []
      times = []
      keypoints3D = []
      exec(Path(filePath).read_text())
      lastTimer = 0
      timerOffset = 0
      state = 0
      for kpID in range(len(keypoints3D)):
         keypointMesh = keypoints3D[kpID]
         timer = times[kpID]
         if keypointMesh != {}:
            d = None
            for keypoint in keypointMesh:
               dU = np.linalg.norm(np.dot(toRobRotation, np.array(keypointMesh[keypoint])) + toRobTranslation - np.array([0.05871,0.0,0.06364]))
               if d == None or dU < d:
                  d = dU
            
            if d != None and lastDistance != d:
               if lastTimer == 0:
                  lastTimer = timer
                  lastDistance = d
         
               if timer-lastTimer > 0:
                  velocity = (d-lastDistance)/(timer-lastTimer)

               #print(d, timer, velocity, personID, i)
               if d > 2:
                  state = 1
               if abs(velocity) < .1 and d < 2 and state == 1:
                  pDistances.append(d)
   
               lastDistance = d
               lastTimer = timer
      
      if len(pDistances) > 0:
         distances.append(np.median(pDistances))
         personIDs.append(personID)
         
   for j in sorted(personIDs):
      print(distances[personIDs.index(j)])


   distances = np.array(distances)

   plt.hist(distances, int(distances.max(axis=0)/resolution), alpha=.8, color=[colors[i]], range=(0.1,1.5))


plt.legend(["Control", "Robot", "Human"])

plt.xlabel("Distance [m]")
plt.ylabel("Count")

rcParams.update({'font.weight': 'bold'})

c = (0,28/255,168/255)
plt.axvline(.45, color=c)
plt.axvline(1.2, color=c, linestyle="dashed")
labels = ["intimate (human scale)", "personal (human scale)"]
plt.text(.45+.07, .3, labels[0], color=c, ha='right', va='bottom', rotation='vertical')
plt.text(1.2+.03, .3, labels[1], color=c, ha='left', va='bottom', rotation='vertical')

c = (0/255,100/255,0/255)
plt.axvline(.16, color=c)
plt.axvline(.42, color=c, linestyle="dashed")
labels = ["intimate (robot scale)", "personal (robot scale)"]
plt.text(.16-.01, .3, labels[0], color=c, ha='right', va='bottom', rotation='vertical')
plt.text(.42-.01, .3, labels[1], color=c, ha='right', va='bottom', rotation='vertical')

plt.savefig('DistanceDistributionNao1.png', format='png', dpi=900)
plt.show()
