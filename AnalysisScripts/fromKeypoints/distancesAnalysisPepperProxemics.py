from __future__ import print_function
#!/usr/bin/env python2
# -*- coding: utf-8 -*-
# Author: Adam Rojik
import numpy as np
import importlib
from pathlib import Path, PosixPath
import glob
import sys,os
import argparse
from matplotlib import pyplot as plt, rcParams
from matplotlib.ticker import MaxNLocator

parser = argparse.ArgumentParser(description='Distances analysis')
parser.add_argument('-i', '--input', type=str,
                    help='input experiment folder', default="/Volumes/HRI/HRI/2019_08-pepper/dataAndVideos/")
args = parser.parse_args()
INPUT = args.input

ax = plt.figure().gca()
ax.yaxis.set_major_locator(MaxNLocator(integer=True))

colors = [None,(1.00,0.,0.00),(0.20,1.00,0.20),(0.30,0.53,1.00)]
for i in [1,2,3]:
   distances = []
   distancesMed = []
   personList = []
   resolution = .1

   for filePath in glob.glob(INPUT+'/*/*-'+str(i)+'-video.avi'):
      personID = int(filePath[len("/Volumes/HRI/HRI/2019_08-pepper/dataAndVideos/"):].split("/")[0])
      if personID in [1,2,3]:
         continue
      
      personList.append(personID)
      data = Path(filePath.replace("-video.avi", "-keypointsDistances.txt")).read_text().strip().split("\n")
      
      lastTimer = 0
      noneCounter = 0
      
      state = 0
      velocity = 0
      discreteDistances = []
      discreteDistancesFloat = []
      timeSpendIn = []
      for s in data:
         row = s.split(" ")
         if row[1] != "None":
            noneCounter = 0
            row = [float(row[0]), float(row[1])]

            if lastTimer == 0:
               lastTimer = row[0]
               lastDistance = row[1]

            if row[0]-lastTimer > 0:
               velocity = (row[1]-lastDistance)/(row[0]-lastTimer)
            
            if row[1] > 2.4:
               state = 1
            
            if state == 1 and velocity < -0.08:
               state = 2
            elif state == 2:
               if abs(velocity) < .08:
                  state = 3
            elif state == 3:
               if abs(velocity) > .08:
                  state = 2

            if state == 3:
               measurementDistance = int(row[1]*40)
               discreteDistancesFloat.append(row[1])
               try:
                  distanceIndex = discreteDistances.index(measurementDistance)
                  timeSpendIn[distanceIndex] += row[0] - lastTimer
               except ValueError:
                  discreteDistances.append(measurementDistance)
                  timeSpendIn.append(row[0] - lastTimer)

            lastTimer = row[0]
            lastDistance = row[1]
         else:
            noneCounter += 1
            if noneCounter > 100:
               state = 0
               velocity = 0
      distancesMed.append(np.median(discreteDistancesFloat))
      distances.append(discreteDistances[np.argmax(timeSpendIn)]/40)

   for j in sorted(personList):
      print(distancesMed[personList.index(j)])
   distances = np.array(distancesMed)
   plt.hist(distances, int(distances.max(axis=0)/resolution), alpha=.8, color=[colors[i]], range=(0.1,1.5))

plt.legend(["Control", "Robot", "Human"])

plt.xlabel("Distance [m]")
plt.ylabel("Count")

rcParams.update({'font.weight': 'bold'})

c = (0,28/255,168/255)
plt.axvline(.45, color=c)
plt.axvline(1.2, color=c, linestyle="dashed")
labels = ["intimate (human scale)", "personal (human scale)"]
plt.text(.45+.07, .3, labels[0], color=c, ha='right', va='bottom', rotation='vertical')
plt.text(1.2+.03, .3, labels[1], color=c, ha='left', va='bottom', rotation='vertical')

c = (0/255,100/255,0/255)
plt.axvline(.3, color=c)
plt.axvline(.81, color=c, linestyle="dashed")
labels = ["intimate (robot scale)", "personal (robot scale)"]
plt.text(.3-.01, .3, labels[0], color=c, ha='right', va='bottom', rotation='vertical')
plt.text(.81-.01, .3, labels[1], color=c, ha='right', va='bottom', rotation='vertical')

plt.savefig('DistanceDistributionPepperProxemics.png', format='png', dpi=900)
plt.show()