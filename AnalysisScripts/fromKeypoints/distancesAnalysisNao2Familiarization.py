from __future__ import print_function
#!/usr/bin/env python2
# -*- coding: utf-8 -*-
# Author: Adam Rojik
import numpy as np
import importlib
from pathlib import Path, PosixPath
import glob
import sys,os
import argparse
from matplotlib import pyplot as plt, rcParams
from matplotlib.ticker import MaxNLocator

parser = argparse.ArgumentParser(description='Distances analysis')
parser.add_argument('-i', '--input', type=str,
                    help='input experiment folder', default="/Volumes/HRI/HRI/2019_08-nao/dataAndVideos")
args = parser.parse_args()
INPUT = args.input

ax = plt.figure().gca()
ax.yaxis.set_major_locator(MaxNLocator(integer=True))

for i in range(1):
   distances = []
   personList = []
   resolution = .1

   for filePath in glob.glob(INPUT+'/*/*'+str(i)+'.avi'):
      personID = int(filePath[len("/Volumes/HRI/HRI/2019_08-nao/dataAndVideos/"):].split("/")[0])
      if personID == 27:
         continue
      
      personList.append(personID)
      data = Path(filePath.replace(".avi", "_distances.txt")).read_text().strip().split("\n")
      
      lastTimer = 0
      noneCounter = 0
      
      discreteDistances = []
      timeSpendIn = []
      for s in data:
         row = s.split(" ")
         if row[1] != "None":
            noneCounter = 0
            row = [float(row[0]), float(row[1])]
            if lastTimer == 0:
               lastTimer = row[0]

            measurementDistance = int(row[1]*100)
            try:
               distanceIndex = discreteDistances.index(measurementDistance)
               timeSpendIn[distanceIndex] += row[0] - lastTimer
            except ValueError:
               discreteDistances.append(measurementDistance)
               timeSpendIn.append(row[0] - lastTimer)

            lastTimer = row[0]
               
      distances.append(discreteDistances[np.argmax(timeSpendIn)]/100)
      print(personID, discreteDistances, timeSpendIn)

   for i in sorted(personList):
      print(distances[personList.index(i)])
   distances = np.array(distances)
   plt.hist(distances, int(distances.max(axis=0)/resolution), alpha=1., color=[(0.30,0.53,1.00)], range=(0.1,2.5))

plt.legend(["Familiarization", "Control", "Robot", "Human"])

plt.xlabel("Distance [m]")
plt.ylabel("Count")

rcParams.update({'font.weight': 'bold'})

c = (0,28/255,168/255)
plt.axvline(.45, color=c)
plt.axvline(1.2, color=c, linestyle="dashed")
labels = ["intimate (human scale)", "personal (human scale)"]
plt.text(.45+.07, .03, labels[0], color=c, ha='right', va='bottom', rotation='vertical')
plt.text(1.2+.03, .03, labels[1], color=c, ha='left', va='bottom', rotation='vertical')

c = (0/255,100/255,0/255)
plt.axvline(.16, color=c)
plt.axvline(.42, color=c, linestyle="dashed")
labels = ["intimate (robot scale)", "personal (robot scale)"]
plt.text(.16-.01, .03, labels[0], color=c, ha='right', va='bottom', rotation='vertical')
plt.text(.42-.01, .03, labels[1], color=c, ha='right', va='bottom', rotation='vertical')

plt.savefig('DistanceDistributionNao2Familiarization.png', format='png', dpi=900)
plt.show()