from __future__ import print_function
#!/usr/bin/env python2
# -*- coding: utf-8 -*-
# Author: Adam Rojik
import numpy as np
import importlib
from pathlib import Path, PosixPath
import sys,os,glob
from matplotlib import pyplot as plt, rcParams
import json
from matplotlib.ticker import MaxNLocator
import pandas as pd

xls = pd.ExcelFile("distances.xlsx")
data = {}
data["NAO-1"] = pd.read_excel(xls, "NAO-1")
data["Pepper"] = pd.read_excel(xls, "Pepper")
data["NAO-2"] = pd.read_excel(xls, "NAO-2")
resolution = .1

ax = plt.figure().gca()
ax.yaxis.set_major_locator(MaxNLocator(integer=True))

experiment = "NAO-2"
outputName = "FamiliarizationNao2"
conditions = "f"
conditionsLegend = {"C":"Control", "R":"Robot", "H":"Human", "f":"Familiarization"}
legend = []

if conditions == "f":
  colors = [(0.30,0.53,1.00)]
  r = (0.1,2.5)
else:
  colors = [(1.00,0.,0.00),(0.20,1.00,0.20),(0.30,0.53,1.00)]
  r = (0.1, 1.5)

for i in range(len(conditions)):
  condition = conditions[i]
  legend.append(conditionsLegend[condition])
  distances = np.array(data[experiment][condition][data[experiment][condition]!="no-data"])

  plt.hist(distances, int(distances.max(axis=0)/resolution), alpha=.8, color=[colors[i]], range=r)

plt.legend(legend)

plt.xlabel("Distance [m]")
plt.ylabel("Count")

rcParams.update({'font.weight': 'bold'})

if experiment == "Pepper":
  c = (0,28/255,168/255)
  plt.axvline(.45, color=c)
  plt.axvline(1.2, color=c, linestyle="dashed")
  labels = ["intimate (human scale)", "personal (human scale)"]
  plt.text(.45+.07, .3, labels[0], color=c, ha='right', va='bottom', rotation='vertical')
  plt.text(1.2+.03, .3, labels[1], color=c, ha='left', va='bottom', rotation='vertical')

  c = (0/255,100/255,0/255)
  plt.axvline(.3, color=c)
  plt.axvline(.81, color=c, linestyle="dashed")
  labels = ["intimate (robot scale)", "personal (robot scale)"]
  plt.text(.3-.01, .3, labels[0], color=c, ha='right', va='bottom', rotation='vertical')
  plt.text(.81-.01, .3, labels[1], color=c, ha='right', va='bottom', rotation='vertical')
else:
  c = (0,28/255,168/255)
  plt.axvline(.45, color=c)
  plt.axvline(1.2, color=c, linestyle="dashed")
  labels = ["intimate (human scale)", "personal (human scale)"]
  plt.text(.45+.07, .3, labels[0], color=c, ha='right', va='bottom', rotation='vertical')
  plt.text(1.2+.03, .3, labels[1], color=c, ha='left', va='bottom', rotation='vertical')

  c = (0/255,100/255,0/255)
  plt.axvline(.16, color=c)
  plt.axvline(.42, color=c, linestyle="dashed")
  labels = ["intimate (robot scale)", "personal (robot scale)"]
  plt.text(.16-.01, .3, labels[0], color=c, ha='right', va='bottom', rotation='vertical')
  plt.text(.42-.01, .3, labels[1], color=c, ha='right', va='bottom', rotation='vertical')

plt.savefig('DistanceDistribution'+outputName+'.png', format='png', dpi=900)
plt.show()
